import React, { useState, useEffect, useContext } from "react";
import { Link } from "react-router-dom";

import {
  Table,
  TableBody,
  TableCell,
  TableRow,
  TableHead,
  Paper,
  Typography,
  Divider,
  IconButton,
  CircularProgress,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Button,
  Tooltip,
  Switch,
  Popper
} from "@material-ui/core";
import Fab from "@material-ui/core/Fab";

import AddIcon from "@material-ui/icons/Add";
import { Create, Delete, Send } from "@material-ui/icons";
import axios from "axios";
import NavigationBar from "./navigationbar";

import { header } from "../authenticate/header";
import { AuthenticatedUserContext } from "../authenticate/authenticatedUserContext";

const styles = {
  tableHeader: {
    backgroundColor: "lavender"
  },
  tableCell: {
    fontSize: 15,
    letterSpacing: 1
  }
};
export default function AnnouncementList() {
  const { authenticatedUser } = useContext(AuthenticatedUserContext);
  const [isLoading, setIsLoading] = useState(true);
  const [announcements, setAnnouncements] = useState([]);
  const [dialogOpen, setDialogOpen] = useState(false);
  const [deleteAnnouncementId, setDeleteAnnouncementId] = useState();
  const [notificationDialog, setNotificationDialog] = useState(false);
  const [notifyAnnouncementId, setNotifyAnnouncementId] = useState();
  const [notifyAnnouncement, setNotifyAnnouncement] = useState({
    eventName: "",
    eventDescription: ""
  });

  useEffect(() => {
    axios
      .get("http://localhost:8080/announcement/data", header)
      .then(response => {
        setAnnouncements(response.data);
        setIsLoading(false);
      })
      .catch(error => {
        console.log(error);
      });
  }, []);

  const handleDialogOpen = id => {
    setDialogOpen(true);
    setDeleteAnnouncementId(id);
  };

  const handleDialogClose = () => {
    setDialogOpen(false);
    setDeleteAnnouncementId();
  };

  const handleNotifyDialogOpen = (id, index) => {
    setNotifyAnnouncement(announcements[index]);
    setNotificationDialog(true);
    setNotifyAnnouncementId(id);
  };

  const handleNotifyDialogClose = () => {
    setNotificationDialog(false);
    setNotifyAnnouncementId();
    setNotifyAnnouncement({});
  };

  const handleDelete = () => {
    axios
      .delete(
        "http://localhost:8080/announcement/delete/" + deleteAnnouncementId,
        header
      )
      .then(response => {
        console.log("Success");

        let tempAnnouncements = announcements.filter(announcement => {
          return announcement.id != deleteAnnouncementId;
        });
        setDialogOpen(false);
        setAnnouncements(tempAnnouncements);
      })
      .catch(error => console.log(error));
  };

  const handleNotifyAnnouncement = () => {
    axios
      .put(
        "http://localhost:8080/announcement/notify/" + notifyAnnouncementId,
        {},
        header
      )
      .then(response => {
        console.log("Success");
        let announcementIndex = announcements.findIndex(
          annoucement => annoucement.id == response.data.id
        );

        setNotificationDialog(false);
        setAnnouncements([
          ...announcements.slice(0, announcementIndex),
          response.data,
          ...announcements.slice(announcementIndex + 1, announcements.length)
        ]);
        console.log(announcements);
      })
      .catch(error => console.log(error));
  };

  return (
    <React.Fragment>
      <NavigationBar />
      <div className="row col-md-12">
        <div className="col-md-6">
          <Typography
            variant="h4"
            gutterBottom
            style={{ marginTop: 100, marginLeft: "20%" }}
          >
            Announcements
          </Typography>
        </div>

        <div className="col-md-6">
          <Link to="/announcement/create">
            <Fab
              color="primary"
              aria-label="add"
              style={{ marginTop: 80, marginLeft: "73%" }}
            >
              <AddIcon />
            </Fab>
          </Link>
        </div>
      </div>
      <div className="row col-md-12">
        <div className="col-md-1" />
        <div className="col-md-10">
          <Paper>
            <Table>
              <TableHead style={styles.tableHeader}>
                <TableRow>
                  <TableCell style={styles.tableCell}>Event Name</TableCell>
                  <TableCell style={styles.tableCell}>Date</TableCell>
                  <TableCell style={styles.tableCell}>Event Type</TableCell>
                  <TableCell style={styles.tableCell}>Notified</TableCell>
                  <TableCell style={styles.tableCell}>Notified Date</TableCell>
                  <TableCell style={styles.tableCell}>
                    {`Edit ${authenticatedUser.role === "ROLE_ADMIN"
                      ? "/Delete"
                      : ""}`}
                  </TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {isLoading ? (
                  <CircularProgress
                    thickness="4"
                    style={{
                      marginTop: 20,
                      marginBottom: 20,
                      marginLeft: "240%"
                    }}
                  />
                ) : (
                  announcements.map((announcement, i) => (
                    <TableRow key={`data${i}`}>
                      <TableCell>{announcement.eventName}</TableCell>
                      <TableCell>{announcement.eventDate}</TableCell>
                      <TableCell>{announcement.event.eventType}</TableCell>
                      <TableCell>
                        <Switch
                          checked={announcement.notified}
                          color="primary"
                        />
                        {announcement.notified === false &&
                        authenticatedUser.role === "ROLE_ADMIN" ? (
                          <Tooltip title="Send Notification">
                            <Send
                              style={{ color: "#007bff" }}
                              onClick={() =>
                                handleNotifyDialogOpen(announcement.id, i)}
                            />
                          </Tooltip>
                        ) : (
                          ""
                        )}
                      </TableCell>
                      <TableCell>{announcement.notifiedDate}</TableCell>
                      <TableCell>
                        <span style={{ marginRight: 5 }}>
                          <Link to={`/announcement/update/${announcement.id}`}>
                            <Create />
                          </Link>
                        </span>

                        {authenticatedUser.role === "ROLE_ADMIN" && (
                          <span style={{ marginLeft: 5 }}>
                            |{" "}
                            <Delete
                              style={{ color: "#007bff" }}
                              onClick={() => handleDialogOpen(announcement.id)}
                            />
                          </span>
                        )}
                      </TableCell>
                      <Dialog
                        open={dialogOpen}
                        onClose={handleDialogClose}
                        aria-labelledby="form-dialog-title"
                      >
                        <DialogTitle id="form-dialog-title">
                          Are you sure ?
                        </DialogTitle>
                        <DialogContent>
                          <DialogContentText>
                            Do you really want to delete this record? This
                            process cannot be undone
                          </DialogContentText>
                        </DialogContent>
                        <DialogActions>
                          <Button onClick={handleDialogClose} color="primary">
                            Cancel
                          </Button>
                          <Button onClick={handleDelete} color="secondary">
                            Delete
                          </Button>
                        </DialogActions>
                      </Dialog>

                      <Dialog
                        open={notificationDialog}
                        onClose={handleNotifyDialogClose}
                        aria-labelledby="form-dialog-title"
                      >
                        <DialogTitle id="form-dialog-title">
                          {notifyAnnouncement.eventName}
                        </DialogTitle>
                        <DialogContent>
                          <DialogContentText>
                            {notifyAnnouncement.eventDescription}
                          </DialogContentText>
                        </DialogContent>
                        <DialogActions>
                          <Button
                            onClick={handleNotifyDialogClose}
                            color="primary"
                          >
                            Cancel
                          </Button>
                          <Button
                            onClick={handleNotifyAnnouncement}
                            color="secondary"
                          >
                            Send
                          </Button>
                        </DialogActions>
                      </Dialog>
                    </TableRow>
                  ))
                )}
              </TableBody>
            </Table>
          </Paper>
        </div>
      </div>
    </React.Fragment>
  );
}
