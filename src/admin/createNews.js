import React from 'react';
import Pdf from '../Documents/456.pdf';

class Download extends React.Component
{
    render() {

        return (
          <div className = "App">
            <a href = {Pdf} target = "_blank">Download Pdf</a>
          </div>
        );
      }
}

export default Download;