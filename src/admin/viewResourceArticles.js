import React, { useState, useEffect, useContext } from "react";
import { Link } from "react-router-dom";
import { header } from "../authenticate/header";

import {
  Table,
  TableBody,
  TableCell,
  TableRow,
  TableHead,
  Paper,
  Typography,
  Divider,
  IconButton,
  CircularProgress,
  Button,
  TextField,
  MenuItem,
  Select,
  FormControl,
  InputLabel,
  OutlinedInput
} from "@material-ui/core";
import Fab from "@material-ui/core/Fab";

import AddIcon from "@material-ui/icons/Add";
import axios from "axios";
import NavigationBar from "./navigationbar";
import fileService from "./fileService";
import { saveAs } from "file-saver";

export default function ViewResourceArticles() {
  const [showResourceDiv, setShowResourcesDiv] = useState(false);
  const [selectedResource, setSelectedResource] = useState("");
  const [resourceArticles, setResourceArticles] = useState([]);
  const [resourceTypeBlank, setResourceTypeBlank] = useState(false);
  const [isLoading, setIsLoading] = useState(false);

  const downloadFile = fileName => {
    fileService
      .getFileFromServer("/resourceArticle/download/", fileName)
      .then(response => {
        console.log("Response", response);
        //extract file name from Content-Disposition header

        var filename = fileService.extractFileName(
          response.headers["content-disposition"]
        );
        console.log("File name", filename);
        //invoke 'Save As' dialog
        saveAs(response.data, filename);
      })
      .catch(function(error) {
        console.log(error);
        if (error.response) {
          console.log("Error", error.response.status);
        } else {
          console.log("Error", error.message);
        }
      });
  };

  const getResource = () => {
    if (selectedResource === "") {
      setShowResourcesDiv(false);
      return;
    }
    setIsLoading(true);
    axios
      .get("http://localhost:8080/resourceArticle/" + selectedResource, header)
      .then(response => {
        setShowResourcesDiv(true);
        setResourceArticles(response.data);
        setIsLoading(false);
      })
      .catch(error => {
        console.log(error);
      });
  };

  const handleChange = event => {
    setSelectedResource(event.target.value);
    if (event.target.value === "") {
      setResourceTypeBlank(true);
    }
    if (event.target.value !== "") {
      setResourceTypeBlank(false);
    }
  };
  return (
    <React.Fragment>
      <NavigationBar />
      <div className="row col-md-12">
        <div className="col-md-1" />
        <div className="col-md-10" style={{ paddingLeft: 40 }}>
          <Paper
            style={{
              marginTop: 100,
              padding: 20,
              paddingBottom: 40,
              marginBottom: 15
            }}
            elevation={3}
          >
            <Typography variant="h5" gutterBottom style={{ paddingLeft: 15 }}>
              Select Type of Resource
            </Typography>
            <FormControl
              variant="outlined"
              style={{
                minWidth: 225,
                marginTop: 14,
                marginLeft: 20,
                marginRight: 20
              }}
            >
              <InputLabel htmlFor="outlined-resourceType-simple">
                Resource Type
              </InputLabel>

              <Select
                value={selectedResource}
                input={
                  <OutlinedInput
                    name="resourceType"
                    id="outlined-resourceType-simple"
                  />
                }
                onChange={handleChange}
              >
                <MenuItem value="">
                  <em>None</em>
                </MenuItem>
                <MenuItem value="academic">Academic Articles</MenuItem>
                <MenuItem value="non-academic">Non-Academic Articles</MenuItem>
              </Select>
            </FormControl>
            <Button
              variant="contained"
              color="primary"
              size="large"
              onClick={getResource}
              style={{ marginLeft: 20, marginTop: 20 }}
            >
              OK
            </Button>
            <br />
            {resourceTypeBlank && (
              <section style={{ color: "red", marginLeft: 25, marginTop: 5 }}>
                Please select resource type
              </section>
            )}
          </Paper>
        </div>
      </div>
      <div className="row col-md-12">
        <div className="col-md-1" />
        <div className="col-md-10" style={{ paddingLeft: 40 }}>
          {showResourceDiv && (
            <Paper style={{ marginTop: 30, padding: 20 }} elevation={3}>
              <Typography
                variant="h5"
                gutterBottom
                style={{ paddingLeft: 25, color: "#25558f" }}
              >
                <strong>
                  {selectedResource === "academic"
                    ? "Academic Resource Articles"
                    : ""}
                  {selectedResource === "non-academic"
                    ? "Non-Academic Resource Articles"
                    : ""}
                </strong>
              </Typography>

              {resourceArticles.map((resource, i) => (
                <section style={{ padding: 25 }}>
                  <Typography
                    variant="h6"
                    gutterBottom
                    style={{ color: "#25558f" }}
                  >
                    {resource.title}
                  </Typography>
                  <Typography
                    variant="h7"
                    gutterBottom
                    style={{ color: "#25558f" }}
                  >
                    {`[Published On: ${resource.publishedDate}]`}
                  </Typography>
                  <p>{resource.description}</p>

                  <a href="#" onClick={() => downloadFile(resource.fileNames)}>
                    Read Full Article
                  </a>
                  <Divider style={{ marginTop: 20, marginBottom: 20 }} />
                </section>
              ))}
              <Paper />
            </Paper>
          )}
        </div>
      </div>
    </React.Fragment>
  );
}
