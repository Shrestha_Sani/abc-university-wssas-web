import React, { useState, useEffect } from "react";
import { Redirect, Link } from "react-router-dom";

import axios from "axios";

import {
  Table,
  TableBody,
  TableCell,
  TableRow,
  TableHead,
  Typography,
  TextField,
  FormControl,
  MenuItem,
  Select,
  InputLabel,
  OutlinedInput,
  Button,
  Paper,
  Icon,
  CircularProgress,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle
} from "@material-ui/core";

import { Create, Delete, Save } from "@material-ui/icons";

import NavigationBar from "./navigationbar";

import { header } from "../authenticate/header";

const styles = {
  container: {
    display: "flex",
    flexWrap: "wrap",
    marginBottom: 35
  },
  textField: {
    marginLeft: 20,
    marginRight: 30,
    minWidth: 229
  },
  topology: {
    flex: 2
  }
};
export default function CreateCourse() {
  const courseData = {
    id: "",
    courseName: "",
    courseCode: ""
  };
  const [courses, setCourses] = useState();
  const [course, setCourse] = useState(courseData);
  const [dialogOpen, setDialogOpen] = useState(false);
  const [deleteCourseId, setDeleteCourseId] = useState();
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    axios
      .get("http://localhost:8080/course/data", header)
      .then(response => {
        setCourses(response.data);
        setIsLoading(false);
        console.log(response.data);
      })
      .catch(error => {
        console.log(error);
      });
  }, []);

  const handleChange = name => event => {
    setCourse({ ...course, [name]: event.target.value });
  };

  const handleSubmitCourse = e => {
    axios
      .post("http://localhost:8080/course/save", course, header)
      .then(response => {
        setCourses([...courses, response.data]);
        setCourse(courseData);
        console.log("Success");
      })
      .catch(error => console.log(error));
    e.preventDefault();
  };

  const handleUpdate = (event, id) => {
    axios
      .put("http://localhost:8080/course/update/" + id, course, header)
      .then(response => {
        console.log("Success");
        const index = courses.findIndex(course => course.id == id);
        const tempCourses = [...courses];
        tempCourses.splice(index, 1, response.data);
        setCourses(tempCourses);
        setCourse(courseData);
      })
      .catch(error => console.log(error));
  };

  const handleDialogOpen = id => {
    setDialogOpen(true);
    setDeleteCourseId(id);
  };

  const handleDialogClose = () => {
    setDialogOpen(false);
    setDeleteCourseId();
  };

  const handleDelete = () => {
    axios
      .delete("http://localhost:8080/course/delete/" + deleteCourseId, header)
      .then(response => {
        console.log("Success");

        let tempCourses = courses.filter(course => {
          return course.id != deleteCourseId;
        });
        setDialogOpen(false);
        setCourses(tempCourses);
      })
      .catch(error => console.log(error));
  };

  return (
    <React.Fragment>
      <NavigationBar />

      <div className="row col-md-12">
        <div className="col-md-1" />
        <div className="col-md-6">
          <Paper
            style={{ padding: 25, marginTop: 100, marginRight: 25 }}
            elevation={3}
          >
            <Typography variant="h6" gutterBottom style={{ marginLeft: "3%" }}>
              Course List
            </Typography>
            <Table>
              <TableHead style={styles.tableHeader}>
                <TableRow>
                  <TableCell style={styles.tableCell}>Course Code</TableCell>
                  <TableCell style={styles.tableCell}>Course Name</TableCell>
                  <TableCell style={styles.tableCell}>Edit/Delete</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {isLoading ? (
                  <CircularProgress
                    thickness={4}
                    style={{
                      marginTop: 20,
                      marginBottom: 20,
                      marginLeft: "120%"
                    }}
                  />
                ) : (
                  courses.map((course, i) => (
                    <TableRow key={`course${i}`}>
                      <TableCell>{course.courseCode}</TableCell>
                      <TableCell>{course.courseName}</TableCell>
                      <TableCell>
                        <span style={{ marginRight: 5 }}>
                          <Create
                            style={{ color: "#007bff" }}
                            onClick={() => setCourse(courses[i])}
                          />
                        </span>
                        <span>|</span>
                        <span style={{ marginLeft: 5 }}>
                          <Delete
                            style={{ color: "#007bff" }}
                            onClick={() => handleDialogOpen(course.id)}
                          />
                        </span>
                      </TableCell>
                      <Dialog
                        open={dialogOpen}
                        onClose={handleDialogClose}
                        aria-labelledby="form-dialog-title"
                      >
                        <DialogTitle id="form-dialog-title">
                          Are you sure ?
                        </DialogTitle>
                        <DialogContent>
                          <DialogContentText>
                            Do you really want to delete this record? This
                            process cannot be undone
                          </DialogContentText>
                        </DialogContent>
                        <DialogActions>
                          <Button onClick={handleDialogClose} color="primary">
                            Cancel
                          </Button>
                          <Button onClick={handleDelete} color="secondary">
                            Delete
                          </Button>
                        </DialogActions>
                      </Dialog>
                    </TableRow>
                  ))
                )}
              </TableBody>
            </Table>
          </Paper>
        </div>
        <div className="col-md-4">
          <Paper
            style={{ padding: 27, marginTop: 100, marginRight: 15 }}
            elevation={3}
          >
            <Typography variant="h6" gutterBottom style={{ marginLeft: "30%" }}>
              Create Course
            </Typography>
            <form style={styles.container} onSubmit={handleSubmitCourse}>
              <TextField
                required
                fullWidth
                id="outlined-name"
                label="Course Code"
                style={styles.textField}
                value={course.courseCode}
                margin="normal"
                variant="outlined"
                onChange={handleChange("courseCode")}
              />

              <TextField
                fullWidth
                id="outlined-uncontrolled"
                label="Course Name"
                value={course.courseName}
                style={styles.textField}
                margin="normal"
                variant="outlined"
                onChange={handleChange("courseName")}
              />

              <div
                className="row col-md-12"
                style={{ marginTop: 25, marginLeft: 14 }}
              >
                {course.id ? (
                  <Button
                    variant="contained"
                    color="primary"
                    onClick={e => handleUpdate(e, course.id)}
                  >
                    Update
                  </Button>
                ) : (
                  <Button variant="contained" color="primary" type="submit">
                    Create
                  </Button>
                )}
              </div>
            </form>
          </Paper>
        </div>
      </div>
    </React.Fragment>
  );
}
