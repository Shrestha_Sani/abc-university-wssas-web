import React, { useState, useEffect, useContext } from "react";
import { Link } from "react-router-dom";
import { header } from "../authenticate/header";

import {
  Table,
  TableBody,
  TableCell,
  TableRow,
  TableHead,
  Paper,
  Typography,
  Divider,
  IconButton,
  CircularProgress,
  Button,
  TextField,
  MenuItem,
  Select,
  FormControl,
  InputLabel,
  OutlinedInput
} from "@material-ui/core";
import Fab from "@material-ui/core/Fab";

import AddIcon from "@material-ui/icons/Add";
import axios from "axios";
import NavigationBar from "./navigationbar";
import DoneIcon from "@material-ui/icons/Done";
import CloseIcon from "@material-ui/icons/Close";
import { AuthenticatedUserContext } from "../authenticate/authenticatedUserContext";

const styles = {
  tableHeader: {
    backgroundColor: "lavender"
  },
  tableCell: {
    fontSize: 15,
    letterSpacing: 1
  },
  textField: {
    marginLeft: 20,
    marginRight: 20,
    minWidth: 229
  }
};
export default function ViewStatistics() {
  const { authenticatedUser } = useContext(AuthenticatedUserContext);
  let currentDate = new Date().toISOString().slice(0, 10);
  const reportData = {
    startDate: currentDate,
    endDate: currentDate
  };

  const [day, setDay] = useState("");
  const [displayStatistics, setDisplayStatistics] = useState(false);
  const [isLoading, setIsLoading] = useState(true);
  const [report, setReport] = useState(reportData);
  const [statistics, setStatistics] = useState({});
  const [showCalendar, setShowCalendar] = useState(false);
  const [fromDateBlank, setFromDateBlank] = useState(false);
  const [toDateBlank, setToDateBlank] = useState(false);

  const getStatisticsByDay = () => {
    if (day === "") return;
    setDisplayStatistics(true);
    setIsLoading(true);
    axios
      .get("http://localhost:8080/view-statistics/" + day, header)
      .then(response => {
        setStatistics(response.data);
        setIsLoading(false);
      })
      .catch(error => {
        console.log(error);
      });
  };

  const getStatisticsByDate = () => {
    if (day === "") return;
    setDisplayStatistics(true);
    setIsLoading(true);
    axios
      .post("http://localhost:8080/view-statistics/dates", report, header)
      .then(response => {
        setStatistics(response.data);
        setIsLoading(false);
      })
      .catch(error => {
        console.log(error);
      });
  };

  const handleChange = name => event => {
    setReport({ ...report, [name]: event.target.value });
    if (event.target.value === "") {
      if (name === "startDate") setFromDateBlank(true);
      if (name === "endDate") setToDateBlank(true);
    }
    if (event.target.value !== "") {
      if (name === "startDate") setFromDateBlank(false);
      if (name === "endDate") setToDateBlank(false);
    }
  };

  const handleSelectChange = e => {
    setDay(e.target.value);
    if (e.target.value === "show-calendar") setShowCalendar(true);
    else setShowCalendar(false);
  };
  return (
    <React.Fragment>
      <NavigationBar />
      <div className="row col-md-12">
        <section style={{ paddingLeft: 40 }}>
          <Paper
            style={{
              marginTop: 100,
              padding: 10,
              marginBottom: 15,
              width: 1224
            }}
            elevation={3}
          >
            <Typography variant="h5" gutterBottom style={{ paddingLeft: 15 }}>
              Generate Report
            </Typography>
            <FormControl
              variant="outlined"
              style={{
                minWidth: 225,
                marginTop: 14,
                marginLeft: 20,
                marginRight: 20
              }}
            >
              <InputLabel htmlFor="outlined-resourceType-simple">
                View Statistics of
              </InputLabel>

              <Select
                value={day}
                input={
                  <OutlinedInput
                    name="resourceType"
                    id="outlined-resourceType-simple"
                  />
                }
                onChange={handleSelectChange}
              >
                <MenuItem value="">
                  <em>None</em>
                </MenuItem>
                <MenuItem value="yesterday">Yesterday</MenuItem>
                <MenuItem value="7-days">Last 7 days</MenuItem>
                <MenuItem value="15-days">Last 15 days</MenuItem>
                <MenuItem value="30-days">Last 30 days</MenuItem>
                <MenuItem value="show-calendar">
                  ****** Select Date ******
                </MenuItem>
              </Select>
            </FormControl>

            {showCalendar === true ? (
              <span>
                <TextField
                  required
                  id="outlined-uncontrolled"
                  label="From Created Date"
                  value={report.startDate}
                  style={styles.textField}
                  margin="normal"
                  variant="outlined"
                  type="date"
                  onChange={handleChange("startDate")}
                />

                <TextField
                  required
                  id="outlined-uncontrolled"
                  label="To Created Date"
                  value={report.endDate}
                  style={styles.textField}
                  margin="normal"
                  variant="outlined"
                  type="date"
                  onChange={handleChange("endDate")}
                />
                <Button
                  variant="contained"
                  color="primary"
                  type="submit"
                  style={{ marginLeft: 20, marginTop: 20 }}
                  onClick={getStatisticsByDate}
                  size="large"
                >
                  View
                </Button>
              </span>
            ) : (
              <Button
                variant="contained"
                color="primary"
                type="submit"
                size="large"
                onClick={getStatisticsByDay}
                style={{ marginLeft: 20, marginTop: 20 }}
              >
                View
              </Button>
            )}
            <div
              className="row col-md-12"
              style={{ marginLeft: 285, marginBottom: 15 }}
            >
              {fromDateBlank === true ? (
                <span style={{ color: "red", marginRight: 110 }}>
                  Please select From date
                </span>
              ) : (
                <span style={{ marginRight: 281 }} />
              )}
              {toDateBlank === true ? (
                <span style={{ color: "red" }}>Please select To date</span>
              ) : (
                ""
              )}
            </div>
          </Paper>
        </section>
      </div>
      {displayStatistics && (
        <section style={{ paddingLeft: 40 }}>
          <div className="row col-md-12">
            <div className="col-md-6">
              <Typography variant="h5" gutterBottom style={{ marginTop: 20 }}>
                Announcement Statistics
              </Typography>
            </div>
          </div>
          <div className="row col-md-12">
            <Paper elevation={3} style={{ marginBottom: 15 }}>
              <Table>
                <TableHead style={styles.tableHeader}>
                  <TableRow>
                    <TableCell style={styles.tableCell}>Created Date</TableCell>
                    <TableCell style={styles.tableCell}>Created By</TableCell>

                    <TableCell style={styles.tableCell}>Username</TableCell>
                    <TableCell style={styles.tableCell}>Event Name</TableCell>
                    <TableCell style={styles.tableCell}>Date</TableCell>
                    <TableCell style={styles.tableCell}>Event Type</TableCell>
                    <TableCell style={styles.tableCell}>Notified</TableCell>
                    <TableCell style={styles.tableCell}>
                      Notified Date
                    </TableCell>
                    <TableCell style={styles.tableCell}>Last Updated</TableCell>
                    <TableCell style={styles.tableCell}>Updated By</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {isLoading ? (
                    <CircularProgress
                      thickness={4}
                      style={{
                        marginTop: 20,
                        marginBottom: 20,
                        marginLeft: "300%"
                      }}
                    />
                  ) : (
                    statistics.announcements.map((announcement, i) => (
                      <TableRow key={`data${i}`}>
                        <TableCell>{announcement.createdDate}</TableCell>
                        <TableCell>{`${announcement.user
                          .firstName} ${announcement.user
                          .lastName}`}</TableCell>
                        <TableCell>{announcement.user.username}</TableCell>
                        <TableCell>{announcement.eventName}</TableCell>
                        <TableCell>{announcement.eventDate}</TableCell>
                        <TableCell>{announcement.event.eventType}</TableCell>
                        <TableCell>
                          {announcement.notified ? (
                            <DoneIcon color="primary" />
                          ) : (
                            <CloseIcon color="secondary" />
                          )}
                        </TableCell>
                        <TableCell>{announcement.notifiedDate}</TableCell>
                        <TableCell>
                          {announcement.updatedDate
                            ? announcement.updatedDate
                            : "-"}
                        </TableCell>
                        <TableCell>
                          {announcement.updatedBy
                            ? announcement.updatedBy.username
                            : "-"}
                        </TableCell>
                      </TableRow>
                    ))
                  )}
                </TableBody>
              </Table>
            </Paper>
          </div>

          <div className="row col-md-12">
            <div className="col-md-6">
              <Typography variant="h5" gutterBottom style={{ marginTop: 20 }}>
                Discussion Forum Statistics
              </Typography>
            </div>
          </div>
          <div className="row col-md-12">
            <Paper elevation={3} style={{ marginBottom: 15 }}>
              <Table>
                <TableHead style={styles.tableHeader}>
                  <TableRow>
                    <TableCell style={styles.tableCell}>Created Date</TableCell>
                    <TableCell style={styles.tableCell}>Started By</TableCell>
                    <TableCell style={styles.tableCell}>Username</TableCell>
                    <TableCell style={styles.tableCell}>Course</TableCell>
                    <TableCell style={styles.tableCell}>
                      Discussion Topic
                    </TableCell>
                    <TableCell style={styles.tableCell}>
                      Total Replies
                    </TableCell>
                    <TableCell style={styles.tableCell}>Last Updated</TableCell>
                    <TableCell style={styles.tableCell}>Updated By</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {isLoading ? (
                    <CircularProgress
                      thickness={4}
                      style={{
                        marginTop: 20,
                        marginBottom: 20,
                        marginLeft: "300%"
                      }}
                    />
                  ) : (
                    statistics.discussionForums.map((discussionForum, i) => (
                      <TableRow key={`data${i}`}>
                        <TableCell>{discussionForum.createdDate}</TableCell>
                        <TableCell>{`${discussionForum.user
                          .firstName} ${discussionForum.user
                          .lastName}`}</TableCell>
                        <TableCell>{discussionForum.user.username}</TableCell>
                        <TableCell>
                          {discussionForum.course.courseName}
                        </TableCell>
                        <TableCell>{discussionForum.subject}</TableCell>
                        <TableCell>{discussionForum.totalPosts}</TableCell>
                        <TableCell>
                          {discussionForum.updatedDate
                            ? discussionForum.updatedDate
                            : "-"}
                        </TableCell>
                        <TableCell>
                          {discussionForum.updatedBy
                            ? discussionForum.updatedBy.username
                            : "-"}
                        </TableCell>
                      </TableRow>
                    ))
                  )}
                </TableBody>
              </Table>
            </Paper>
          </div>

          <div className="row col-md-12">
            <div className="col-md-6">
              <Typography variant="h5" gutterBottom style={{ marginTop: 20 }}>
                Resource Articles Statistics
              </Typography>
            </div>
          </div>
          <div className="row col-md-12">
            <Paper elevation={3} style={{ marginBottom: 15 }}>
              <Table>
                <TableHead style={styles.tableHeader}>
                  <TableRow>
                    <TableCell style={styles.tableCell}>Created Date</TableCell>
                    <TableCell style={styles.tableCell}>Created By</TableCell>
                    <TableCell style={styles.tableCell}>Username</TableCell>
                    <TableCell style={styles.tableCell}>
                      Resource Type
                    </TableCell>
                    <TableCell style={styles.tableCell}>
                      Published Date
                    </TableCell>
                    <TableCell style={styles.tableCell}>Title</TableCell>
                    <TableCell style={styles.tableCell}>Last Updated</TableCell>
                    <TableCell style={styles.tableCell}>Updated By</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {isLoading ? (
                    <CircularProgress
                      thickness={4}
                      style={{
                        marginTop: 20,
                        marginBottom: 20,
                        marginLeft: "300%"
                      }}
                    />
                  ) : (
                    statistics.resourceArticles.map((resourceArticle, i) => (
                      <TableRow key={`data${i}`}>
                        <TableCell>{resourceArticle.createdDate}</TableCell>
                        <TableCell>{`${resourceArticle.user
                          .firstName} ${resourceArticle.user
                          .lastName}`}</TableCell>
                        <TableCell>{resourceArticle.user.username}</TableCell>
                        <TableCell>{resourceArticle.resourceType}</TableCell>
                        <TableCell>{resourceArticle.publishedDate}</TableCell>
                        <TableCell>{resourceArticle.title}</TableCell>
                        <TableCell>
                          {resourceArticle.updatedDate
                            ? resourceArticle.updatedDate
                            : "-"}
                        </TableCell>
                        <TableCell>
                          {resourceArticle.updatedBy
                            ? resourceArticle.updatedBy.username
                            : "-"}
                        </TableCell>
                      </TableRow>
                    ))
                  )}
                </TableBody>
              </Table>
            </Paper>
          </div>
        </section>
      )}
    </React.Fragment>
  );
}
