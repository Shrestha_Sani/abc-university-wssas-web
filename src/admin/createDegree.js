import React, { useState, useEffect } from "react";
import { Redirect, Link } from "react-router-dom";

import axios from "axios";

import {
  Table,
  TableBody,
  TableCell,
  TableRow,
  TableHead,
  Typography,
  TextField,
  FormControl,
  MenuItem,
  Select,
  InputLabel,
  OutlinedInput,
  Button,
  Paper,
  Icon,
  CircularProgress,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle
} from "@material-ui/core";

import { Create, Delete, Save } from "@material-ui/icons";
import NavigationBar from "./navigationbar";
import { header } from "../authenticate/header";

const styles = {
  container: {
    display: "flex",
    flexWrap: "wrap",
    marginBottom: 35
  },
  textField: {
    marginLeft: 20,
    marginRight: 30,
    minWidth: 229
  },
  topology: {
    flex: 2
  }
};

export default function CreateDegree() {
  const degreeData = {
    id: "",
    degreeName: "",
    duration: ""
  };

  const [degrees, setDegrees] = useState();
  const [degree, setDegree] = useState(degreeData);
  const [dialogOpen, setDialogOpen] = useState(false);
  const [deleteDegreeId, setDeleteDegreeId] = useState();
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    axios
      .get("http://localhost:8080/degree/data", header)
      .then(response => {
        setDegrees(response.data);
        setIsLoading(false);
        console.log(response.data);
      })
      .catch(error => {
        console.log(error);
      });
  }, []);

  const handleChange = name => event => {
    setDegree({ ...degree, [name]: event.target.value });
  };

  const handleSubmitDegree = e => {
    axios
      .post("http://localhost:8080/degree/save", degree, header)
      .then(response => {
        setDegrees([...degrees, response.data]);
        setDegree(degreeData);
        console.log("Success");
      })
      .catch(error => console.log(error));
    e.preventDefault();
  };

  const handleUpdate = (event, id) => {
    axios
      .put("http://localhost:8080/degree/update/" + id, degree, header)
      .then(response => {
        console.log("Success");
        const index = degrees.findIndex(degree => degree.id == id);
        const tempDegrees = [...degrees];
        tempDegrees.splice(index, 1, response.data);
        setDegrees(tempDegrees);
        setDegree(degreeData);
      })
      .catch(error => console.log(error));
  };
  const handleDialogOpen = id => {
    setDialogOpen(true);
    setDeleteDegreeId(id);
  };

  const handleDialogClose = () => {
    setDialogOpen(false);
    setDeleteDegreeId();
  };

  const handleDelete = () => {
    axios
      .delete("http://localhost:8080/degree/delete/" + deleteDegreeId, header)
      .then(response => {
        console.log("Success");

        let tempDegrees = degrees.filter(degree => {
          return degree.id != deleteDegreeId;
        });
        setDialogOpen(false);
        setDegrees(tempDegrees);
      })
      .catch(error => console.log(error));
  };
  return (
    <React.Fragment>
      <NavigationBar />

      <div className="row col-md-12">
        <div className="col-md-1" />
        <div className="col-md-6">
          <Paper
            style={{ padding: 25, marginTop: 100, marginRight: 25 }}
            elevation={3}
          >
            <Typography variant="h6" gutterBottom style={{ marginLeft: "3%" }}>
              Degree List
            </Typography>
            <Table>
              <TableHead style={styles.tableHeader}>
                <TableRow>
                  <TableCell style={styles.tableCell}>Degree Name</TableCell>
                  <TableCell style={styles.tableCell}>
                    Degree Duration
                  </TableCell>
                  <TableCell style={styles.tableCell}>Edit/Delete</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {isLoading ? (
                  <CircularProgress
                    thickness={4}
                    style={{
                      marginTop: 20,
                      marginBottom: 20,
                      marginLeft: "120%"
                    }}
                  />
                ) : (
                  degrees.map((degree, i) => (
                    <TableRow key={`degree${i}`}>
                      <TableCell>{degree.degreeName}</TableCell>
                      <TableCell>{degree.duration}</TableCell>
                      <TableCell>
                        <span style={{ marginRight: 5 }}>
                          <Create
                            style={{ color: "#007bff" }}
                            onClick={() => setDegree(degrees[i])}
                          />
                        </span>
                        <span>|</span>
                        <span style={{ marginLeft: 5 }}>
                          <Delete
                            style={{ color: "#007bff" }}
                            onClick={() => handleDialogOpen(degree.id)}
                          />
                        </span>
                      </TableCell>
                      <Dialog
                        open={dialogOpen}
                        onClose={handleDialogClose}
                        aria-labelledby="form-dialog-title"
                      >
                        <DialogTitle id="form-dialog-title">
                          Are you sure ?
                        </DialogTitle>
                        <DialogContent>
                          <DialogContentText>
                            Do you really want to delete this record? This
                            process cannot be undone
                          </DialogContentText>
                        </DialogContent>
                        <DialogActions>
                          <Button onClick={handleDialogClose} color="primary">
                            Cancel
                          </Button>
                          <Button onClick={handleDelete} color="secondary">
                            Delete
                          </Button>
                        </DialogActions>
                      </Dialog>
                    </TableRow>
                  ))
                )}
              </TableBody>
            </Table>
          </Paper>
        </div>
        <div className="col-md-4">
          <Paper
            style={{ padding: 27, marginTop: 100, marginRight: 15 }}
            elevation={3}
          >
            <Typography variant="h6" gutterBottom style={{ marginLeft: "30%" }}>
              Create Degree
            </Typography>
            <form style={styles.container} onSubmit={handleSubmitDegree}>
              <TextField
                required
                fullWidth
                id="outlined-name"
                label="Degree Name"
                style={styles.textField}
                value={degree.degreeName}
                margin="normal"
                variant="outlined"
                onChange={handleChange("degreeName")}
              />
              <TextField
                fullWidth
                id="outlined-uncontrolled"
                label="Degree Duration"
                value={degree.duration}
                style={styles.textField}
                margin="normal"
                variant="outlined"
                onChange={handleChange("duration")}
              />
              <div
                className="row col-md-12"
                style={{ marginTop: 25, marginLeft: 14 }}
              >
                {degree.id ? (
                  <Button
                    variant="contained"
                    color="primary"
                    onClick={e => handleUpdate(e, degree.id)}
                  >
                    Update
                  </Button>
                ) : (
                  <Button variant="contained" color="primary" type="submit">
                    Create
                  </Button>
                )}
              </div>
            </form>
          </Paper>
        </div>
      </div>
    </React.Fragment>
  );
}
