import React, { useState, useEffect } from "react";
import axios from "axios";
import { Typography } from "@material-ui/core";
import NavigationBar from "./navigationbar";

import staffLogo from "../images/staff.png";
import studentLogo from "../images/student.png";
import courseLogo from "../images/course.png";
import announcementLogo from "../images/announcement.png";
import discussionForumLogo from "../images/discussionForum.png";
import resources from "../images/resources.png";
import { header } from "../authenticate/header";

const styles = {
  container: {
    display: "flex",
    flexWrap: "wrap",
    marginBottom: 35
  },
  box: {
    width: 230,
    height: 200,
    marginLeft: 20,
    marginRight: 30,
    marginBottom: 30,
    paddingLeft: 40,
    borderRadius: 25
  },
  count: {
    marginTop: -17,
    marginLeft: "31%",
    fontSize: 43
  },
  info: {
    fontSize: 21,
    marginTop: -12,
    marginLeft: "19%"
  }
};

export default function Dashboard() {
  const [totalStudent, setTotalStudent] = useState(0);
  const [totalStaff, setTotalStaff] = useState(0);
  const [totalCourse, setTotalCourse] = useState(0);
  const [totalAnnouncement, setTotalAnnouncement] = useState(0);
  const [totalDiscussionForum, setTotalDiscussionForum] = useState(0);
  const [totalResourceArticle, setTotalResourceArticle] = useState(0);

  useEffect(() => {
    axios
      .get("http://localhost:8080/user/student/count", header)
      .then(response => {
        setTotalStudent(response.data);
      })
      .catch(error => {
        console.log(error);
      });

    axios
      .get("http://localhost:8080/user/staff/count", header)
      .then(response => {
        setTotalStaff(response.data);
      })
      .catch(error => {
        console.log(error);
      });

    axios
      .get("http://localhost:8080/course/count", header)
      .then(response => {
        setTotalCourse(response.data);
      })
      .catch(error => {
        console.log(error);
      });

    axios
      .get("http://localhost:8080/discussionForum/count", header)
      .then(response => {
        setTotalDiscussionForum(response.data);
      })
      .catch(error => {
        console.log(error);
      });

    axios
      .get("http://localhost:8080/announcement/new/count", header)
      .then(response => {
        setTotalAnnouncement(response.data);
      })
      .catch(error => {
        console.log(error);
      });

    axios
      .get("http://localhost:8080/resourceArticle/count", header)
      .then(response => {
        setTotalResourceArticle(response.data);
      })
      .catch(error => {
        console.log(error);
      });
  }, []);

  return (
    <div>
      <NavigationBar />
      <Typography
        variant="h4"
        gutterBottom
        style={{
          marginTop: 90,
          marginLeft: "35%",
          color: "#6f42c1",
          marginBottom: 30
        }}
      >
        <strong>
          <p style={{ letterSpacing: 5 }}>WELCOME ADMIN</p>
        </strong>
      </Typography>
      <div className="row col-md-12">
        <div className="col-md-2" />
        <div className="col-md-9" style={styles.container}>
          <div style={{ ...styles.box, backgroundColor: "#c4bca7" }}>
            <div>
              <img src={staffLogo} width="150" height="120" />
            </div>
            <div style={styles.count}>{totalStaff}</div>
            <div style={{ ...styles.info, paddingLeft: 18 }}>Staff</div>
          </div>

          <div style={{ ...styles.box, backgroundColor: "#d9d0b8" }}>
            <div>
              <img src={studentLogo} width="150" height="120" />
            </div>
            <div style={styles.count}>{totalStudent}</div>
            <div style={styles.info}>Students</div>
          </div>

          <div
            style={{
              ...styles.box,
              backgroundColor: "#c4bca7"
            }}
          >
            <div>
              <img src={courseLogo} width="150" height="120" />
            </div>
            <div style={styles.count}>{totalCourse}</div>
            <div style={{ ...styles.info, paddingLeft: 10 }}>Course</div>
          </div>

          <div
            style={{
              ...styles.box,
              backgroundColor: "#d9d0b8",
              paddingLeft: 55,
              paddingTop: 10
            }}
          >
            <div>
              <img src={announcementLogo} width="120" height="90" />
            </div>
            <div style={{ ...styles.count, marginLeft: "26%" }}>
              {totalAnnouncement}
            </div>
            <div
              style={{
                ...styles.info,
                fontSize: 18,
                marginBottom: 10,
                paddingLeft: 12
              }}
            >
              New
            </div>
            <div style={{ ...styles.info, fontSize: 18, marginLeft: 0 }}>
              Announcements
            </div>
          </div>

          <div
            style={{
              ...styles.box,
              backgroundColor: "#c4bca7",
              paddingLeft: 55,
              paddingTop: 10
            }}
          >
            <div>
              <img src={discussionForumLogo} width="120" height="90" />
            </div>
            <div style={{ ...styles.count, marginLeft: "30%" }}>
              {totalDiscussionForum}
            </div>
            <div
              style={{
                ...styles.info,
                fontSize: 18,
                marginBottom: 10,
                marginLeft: "12%"
              }}
            >
              Discussion
            </div>
            <div style={{ ...styles.info, fontSize: 18, marginLeft: "22%" }}>
              Topics
            </div>
          </div>

          <div
            style={{
              ...styles.box,
              backgroundColor: "#d9d0b8",
              paddingLeft: 68,
              paddingTop: 10
            }}
          >
            <div>
              <img src={resources} width="100" height="90" />
            </div>
            <div style={{ ...styles.count, marginLeft: "24%" }}>
              {totalResourceArticle}
            </div>
            <div
              style={{
                ...styles.info,
                fontSize: 18,
                marginBottom: 10,
                marginLeft: "8%"
              }}
            >
              Resource
            </div>
            <div style={{ ...styles.info, fontSize: 18, marginLeft: "11%" }}>
              Articles
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
