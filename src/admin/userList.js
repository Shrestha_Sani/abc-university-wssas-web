import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { header } from "../authenticate/header";

import {
  Table,
  TableBody,
  TableCell,
  TableRow,
  TableHead,
  Paper,
  Typography,
  Divider,
  IconButton,
  CircularProgress,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Button
} from "@material-ui/core";
import Fab from "@material-ui/core/Fab";

import AddIcon from "@material-ui/icons/Add";
import { Create, Delete } from "@material-ui/icons";
import axios from "axios";
import NavigationBar from "./navigationbar";

const styles = {
  tableHeader: {
    backgroundColor: "lavender"
  },
  tableCell: {
    fontSize: 15,
    letterSpacing: 1
  }
};
export default function UserList() {
  const [isLoading, setIsLoading] = useState(true);
  const [users, setUsers] = useState([]);
  const [dialogOpen, setDialogOpen] = useState(false);
  const [deleteUserId, setDeleteUserId] = useState();

  useEffect(() => {
    axios
      .get("http://localhost:8080/user/data", header)
      .then(response => {
        setUsers(response.data);
        setIsLoading(false);
      })
      .catch(error => {
        console.log(error);
      });
  }, []);

  const handleDialogOpen = id => {
    setDialogOpen(true);
    setDeleteUserId(id);
  };

  const handleDialogClose = () => {
    setDialogOpen(false);
    setDeleteUserId();
  };

  const handleDelete = () => {
    axios
      .delete("http://localhost:8080/user/delete/" + deleteUserId, header)
      .then(response => {
        console.log("Success");

        let tempUsers = users.filter(user => {
          return user.id != deleteUserId;
        });
        setDialogOpen(false);
        setUsers(tempUsers);
      })
      .catch(error => console.log(error));
  };
  return (
    <React.Fragment>
      <NavigationBar />
      <div className="row col-md-12">
        <div className="col-md-6">
          <Typography
            variant="h4"
            gutterBottom
            style={{ marginTop: 100, marginLeft: "20%" }}
          >
            Users
          </Typography>
        </div>

        <div className="col-md-6">
          <Link to="/user/create">
            <Fab
              color="primary"
              aria-label="add"
              style={{ marginTop: 80, marginLeft: "73%" }}
            >
              <AddIcon />
            </Fab>
          </Link>
        </div>
      </div>
      <div className="row col-md-12">
        <div className="col-md-1" />
        <div className="col-md-10">
          <Paper>
            <Table>
              <TableHead style={styles.tableHeader}>
                <TableRow>
                  <TableCell style={styles.tableCell}>Name</TableCell>
                  <TableCell style={styles.tableCell}>Address</TableCell>
                  <TableCell style={styles.tableCell}>Email</TableCell>
                  <TableCell style={styles.tableCell}>User ID</TableCell>
                  <TableCell style={styles.tableCell}>Edit/Delete</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {isLoading ? (
                  <CircularProgress
                    thickness={4}
                    style={{
                      marginTop: 20,
                      marginBottom: 20,
                      marginLeft: "240%"
                    }}
                  />
                ) : (
                  users.map((user, i) => (
                    <TableRow key={`data${i}`}>
                      <TableCell>
                        {`${user.firstName} ${user.middleName} ${user.lastName}`}
                      </TableCell>
                      <TableCell>{`${user.address.address} ${user.address
                        .state} ${user.address.postCode}`}</TableCell>
                      <TableCell>{user.email}</TableCell>
                      <TableCell>{user.username}</TableCell>
                      <TableCell>
                        <span style={{ marginRight: 5 }}>
                          <Link to={`/user/update/${user.id}`}>
                            <Create />
                          </Link>
                        </span>
                        <span>|</span>
                        <span style={{ marginLeft: 5 }}>
                          <Delete
                            style={{ color: "#007bff" }}
                            onClick={() => handleDialogOpen(user.id)}
                          />
                        </span>
                      </TableCell>
                    </TableRow>
                  ))
                )}
              </TableBody>
            </Table>
            <Dialog
              open={dialogOpen}
              onClose={handleDialogClose}
              aria-labelledby="form-dialog-title"
            >
              <DialogTitle id="form-dialog-title">Are you sure ?</DialogTitle>
              <DialogContent>
                <DialogContentText>
                  Do you really want to delete this record? This process cannot
                  be undone
                </DialogContentText>
              </DialogContent>
              <DialogActions>
                <Button onClick={handleDialogClose} color="primary">
                  Cancel
                </Button>
                <Button onClick={handleDelete} color="secondary">
                  Delete
                </Button>
              </DialogActions>
            </Dialog>
          </Paper>
        </div>
      </div>
    </React.Fragment>
  );
}
