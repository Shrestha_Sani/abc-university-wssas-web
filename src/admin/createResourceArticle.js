import React, { useState, useEffect, useContext } from "react";
import { Redirect } from "react-router-dom";
import axios from "axios";

import {
  Typography,
  TextField,
  FormControl,
  MenuItem,
  Select,
  InputLabel,
  InputBase,
  withStyles,
  OutlinedInput,
  Button,
  Paper
} from "@material-ui/core";

import NavigationBar from "./navigationbar";
import FileUpload from "./fileUpload";
import CreateNews from "./createNews";
import { log } from "util";
import fileService from "./fileService";

import { header } from "../authenticate/header";
import { AuthenticatedUserContext } from "../authenticate/authenticatedUserContext";

const styles = {
  container: {
    display: "flex",
    flexWrap: "wrap",
    marginBottom: 35
  },
  textField: {
    marginLeft: 20,
    marginRight: 30,
    minWidth: 350
  },
  topology: {
    flex: 2
  }
};

export default function CreateResourceArticle(props) {
  const currentDate = new Date();
  const { authenticatedUser } = useContext(AuthenticatedUserContext);
  const resourceArticleData = {
    id: "",
    resourceType: "",
    title: "",
    description: "",
    user: {
      username: authenticatedUser.username
    },
    publishedDate: currentDate
  };
  const formData = new FormData();

  const [dbResourceArticleData, setDbResourceArticleData] = useState([]);
  const [resourceArticle, setResourceArticle] = useState(resourceArticleData);
  const [showFileSizeError, setShowFileSizeError] = useState(false);
  const [isLoading, setIsLoading] = useState(true);
  const [navigate, setNavigation] = useState(false);

  useEffect(() => {
    if (!props.id) {
      return;
    }
    axios
      .get("http://localhost:8080/resourceArticle/" + props.id, header)
      .then(response => {
        setResourceArticle(response.data);
        // setIsLoading(false);

        console.log(response.data);
      })
      .catch(error => {
        console.log(error);
      });
  }, []);

  const handleChange = name => event => {
    if ([name] == "file") {
      console.log("length", event.target.files.length);

      for (let i = 0; i < event.target.files.length; i++) {
        if (event.target.files[0].size / 1024 / 1024 > 2) {
          setShowFileSizeError(true);
          return;
        } else {
          setShowFileSizeError(false);
        }
        formData.append(
          "file",
          event.target.files[i],
          event.target.files[i].name
        );
        console.log(event.target.files[i].name);
      }

      console.log("formData", formData);
    } else
      setResourceArticle({ ...resourceArticle, [name]: event.target.value });
  };

  const handleUpdate = () => {
    let updatedBy = {
      ...resourceArticle.updatedBy,
      username: authenticatedUser.username
    };
    let tempResourceArticle = { ...resourceArticle, updatedBy };
    axios
      .put(
        "http://localhost:8080/resourceArticle/update/" + props.id,
        tempResourceArticle,
        header
      )
      .then(response => {
        console.log("Success");
        setNavigation(true);
      })
      .catch(error => console.log(error));
  };

  const handleSubmitResource = event => {
    let json = "{";
    if (resourceArticle.resourceType.trim() !== "")
      json = json + '"resourceType":"' + resourceArticle.resourceType + '",';
    if (resourceArticle.title.trim() !== "")
      json = json + '"title":"' + resourceArticle.title + '",';
    if (resourceArticle.description.trim() !== "")
      json += '"description":"' + resourceArticle.description + '",';
    if (resourceArticle.publishedDate.trim() !== "") {
      json += '"publishedDate":"' + resourceArticle.publishedDate + '",';
    }

    json += '"user":{"username":"' + authenticatedUser.username + '"}}';

    formData.append("model", json);
    console.log("submitting");
    fileService
      .uploadFileToServer("/resourceArticle/save", formData)
      .then(response => {
        setDbResourceArticleData([...dbResourceArticleData, response.data]);
        console.log("Successly created resource articles");
        setResourceArticle(resourceArticleData);
        setNavigation(true);
      })
      .catch(error => console.log(error));
    event.preventDefault();
  };

  return (
    <React.Fragment>
      <NavigationBar />

      {navigate && (
        <div>
          <Redirect to="/resource-article/list" />
        </div>
      )}
      <Typography
        variant="h4"
        gutterBottom
        style={{ marginTop: 100, marginLeft: "40%" }}
      >
        Create Resource Article
      </Typography>
      <div className="col-md-2" />

      <div className="row col-md-12">
        <div className="col-md-2" />
        <div className="col-md-9">
          <Paper style={{ padding: 27, marginRight: 25 }} elevation={3}>
            <form style={styles.container} onSubmit={handleSubmitResource}>
              <FormControl
                variant="outlined"
                style={{
                  minWidth: 393,
                  marginTop: 14,
                  marginLeft: 20,
                  marginRight: 30
                }}
              >
                <InputLabel htmlFor="outlined-resourceType-simple">
                  Select Resource Type
                </InputLabel>

                <Select
                  value={resourceArticle.resourceType}
                  input={
                    <OutlinedInput
                      name="resourceType"
                      id="outlined-resourceType-simple"
                    />
                  }
                  onChange={handleChange("resourceType")}
                >
                  <MenuItem value="">
                    <em>None</em>
                  </MenuItem>
                  <MenuItem value="News">News</MenuItem>
                  <MenuItem value="Academic">Academic</MenuItem>
                  <MenuItem value="Non-academic">Non-Academic</MenuItem>
                </Select>
              </FormControl>

              <TextField
                required
                id="outlined-uncontrolled"
                label="Published Date"
                value={resourceArticle.publishedDate}
                style={styles.textField}
                margin="normal"
                variant="outlined"
                type="date"
                onChange={handleChange("publishedDate")}
              />
              <TextField
                required
                id="outlined-name"
                label="Resource Title"
                style={styles.textField}
                value={resourceArticle.title}
                margin="normal"
                variant="outlined"
                onChange={handleChange("title")}
                fullWidth
              />

              <TextField
                required
                fullWidth
                id="outlined-required"
                label="Resource Description"
                value={resourceArticle.description}
                style={styles.textField}
                margin="normal"
                variant="outlined"
                multiline={true}
                rows={5}
                rowsMax={7}
                onChange={handleChange("description")}
              />
              {/* <FileUpload onChange={handleChange("file")} /> */}
              <input
                type="file"
                className="form-control"
                name="file"
                onChange={handleChange("file")}
              />
              {showFileSizeError && (
                <span style={{ color: "red", marginLeft: 15, marginTop: 5 }}>
                  File size exceeds 2MB. File exceeding 2MB will not be
                  uploaded.
                </span>
              )}
              <div
                className="row col-md-12"
                style={{ marginTop: 25, marginLeft: 14 }}
              >
                {resourceArticle.id ? (
                  <Button
                    variant="contained"
                    color="primary"
                    onClick={() => handleUpdate()}
                  >
                    Update
                  </Button>
                ) : (
                  <Button variant="contained" color="primary" type="submit">
                    Create
                  </Button>
                )}
              </div>
            </form>
          </Paper>
        </div>
      </div>
    </React.Fragment>
  );
}
