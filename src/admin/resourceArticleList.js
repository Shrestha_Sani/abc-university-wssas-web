import React, { useState, useEffect, Component } from "react";
import { Link } from "react-router-dom";
import { render } from "react-dom";
import fileService from "./fileService";
import { saveAs } from "file-saver";

import {
  Table,
  TableBody,
  TableCell,
  TableRow,
  TableHead,
  Paper,
  Typography,
  Divider,
  IconButton,
  CircularProgress,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Button
} from "@material-ui/core";
import Fab from "@material-ui/core/Fab";

import AddIcon from "@material-ui/icons/Add";
import { Create, Delete } from "@material-ui/icons";
import axios from "axios";
import NavigationBar from "./navigationbar";
import { header } from "../authenticate/header";

const styles = {
  tableHeader: {
    backgroundColor: "lavender"
  },
  tableCell: {
    fontSize: 15,
    letterSpacing: 1
  }
};

export default function ResourceArticleList() {
  const [isLoading, setIsLoading] = useState(true);
  const [resourceArticles, setResourceArticles] = useState([]);
  const [dialogOpen, setDialogOpen] = useState(false);
  const [deleteResourceArticleId, setDeleteResourceArticleId] = useState();

  useEffect(() => {
    axios
      .get("http://localhost:8080/resourceArticle/data", header)
      .then(response => {
        setResourceArticles(response.data);
        setIsLoading(false);
      })
      .catch(error => {
        console.log(error);
      });
  }, []);

  const handleDialogOpen = id => {
    setDialogOpen(true);
    setDeleteResourceArticleId(id);
  };
  const splitNames = files => {
    if (files != "" && files != null && files !== undefined) {
      let list = files.split(",");
      return list;
    }
    return null;
  };
  const handleDialogClose = () => {
    setDialogOpen(false);
    setDeleteResourceArticleId();
  };

  const handleDelete = () => {
    axios
      .delete(
        "http://localhost:8080/resourceArticle/delete/" +
          deleteResourceArticleId,
        header
      )
      .then(response => {
        console.log("Success");

        let tempResourceArticles = resourceArticles.filter(resourceArticle => {
          return resourceArticle.id != deleteResourceArticleId;
        });
        setDialogOpen(false);
        setResourceArticles(tempResourceArticles);
      })
      .catch(error => console.log(error));
  };

  const downloadFile = fileName => {
    fileService
      .getFileFromServer("/resourceArticle/download/", fileName)
      .then(response => {
        console.log("Response", response);
        //extract file name from Content-Disposition header

        var filename = fileService.extractFileName(
          response.headers["content-disposition"]
        );
        console.log("File name", filename);
        //invoke 'Save As' dialog
        saveAs(response.data, filename);
      })
      .catch(function(error) {
        console.log(error);
        if (error.response) {
          console.log("Error", error.response.status);
        } else {
          console.log("Error", error.message);
        }
      });
  };

  return (
    <React.Fragment>
      <NavigationBar />

      <div className="row col-md-12">
        <div className="col-md-6">
          <Typography
            variant="h4"
            gutterBottom
            style={{ marginTop: 100, marginLeft: "20%" }}
          >
            Resource Articles
          </Typography>
        </div>

        <div className="col-md-6">
          <Link to="/resource-article/create">
            <Fab
              color="primary"
              aria-label="add"
              style={{ marginTop: 80, marginLeft: "73%" }}
            >
              <AddIcon />
            </Fab>
          </Link>
        </div>
      </div>

      <div className="row col-md-12">
        <div className="col-md-1" />
        <div className="col-md-10">
          <Paper>
            <Table>
              <TableHead style={styles.tableHeader}>
                <TableRow>
                  <TableCell style={styles.tableCell}>Resource Type</TableCell>
                  <TableCell style={styles.tableCell}>Published Date</TableCell>
                  <TableCell style={styles.tableCell}>Title</TableCell>
                  <TableCell style={styles.tableCell}>Files</TableCell>
                  <TableCell style={styles.tableCell}>Edit/Delete</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {isLoading ? (
                  <CircularProgress
                    thickness="4"
                    style={{
                      marginTop: 20,
                      marginBottom: 20,
                      marginLeft: "105%"
                    }}
                  />
                ) : (
                  resourceArticles.map((resourceArticle, i) => (
                    <TableRow key={`data${i}`}>
                      <TableCell>{resourceArticle.resourceType}</TableCell>
                      <TableCell>{resourceArticle.publishedDate}</TableCell>
                      <TableCell>{resourceArticle.title}</TableCell>
                      <TableCell>
                        {resourceArticle.fileNames != null
                          ? splitNames(resourceArticle.fileNames).map(file => (
                              <span>
                                <a
                                  href="#"
                                  style={{ color: "blue" }}
                                  onClick={() => downloadFile(file)}
                                >
                                  {`${file}`}
                                </a>{" "}
                              </span>
                            ))
                          : ""}
                      </TableCell>
                      <TableCell>
                        <span style={{ marginRight: 5 }}>
                          <Link
                            to={`/resource-article/update/${resourceArticle.id}`}
                          >
                            <Create />
                          </Link>
                        </span>
                        <span>|</span>
                        <span style={{ marginLeft: 5 }}>
                          <Delete
                            style={{ color: "#007bff" }}
                            onClick={() => handleDialogOpen(resourceArticle.id)}
                          />
                        </span>
                      </TableCell>
                    </TableRow>
                  ))
                )}
              </TableBody>
            </Table>
          </Paper>
          <Dialog
            open={dialogOpen}
            onClose={handleDialogClose}
            aria-labelledby="form-dialog-title"
          >
            <DialogTitle id="form-dialog-title">Are you sure ?</DialogTitle>
            <DialogContent>
              <DialogContentText>
                Do you really want to delete this record? This process cannot be
                undone
              </DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button onClick={handleDialogClose} color="primary">
                Cancel
              </Button>
              <Button onClick={handleDelete} color="secondary">
                Delete
              </Button>
            </DialogActions>
          </Dialog>
        </div>
      </div>
    </React.Fragment>
  );
}
