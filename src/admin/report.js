import React, { useState, useEffect, useContext } from "react";
import { Link } from "react-router-dom";
import { header } from "../authenticate/header";

import {
  Table,
  TableBody,
  TableCell,
  TableRow,
  TableHead,
  Paper,
  Typography,
  Divider,
  IconButton,
  CircularProgress,
  Button,
  TextField
} from "@material-ui/core";
import Fab from "@material-ui/core/Fab";

import AddIcon from "@material-ui/icons/Add";
import axios from "axios";
import NavigationBar from "./navigationbar";
import DoneIcon from "@material-ui/icons/Done";
import CloseIcon from "@material-ui/icons/Close";
import { AuthenticatedUserContext } from "../authenticate/authenticatedUserContext";

const styles = {
  tableHeader: {
    backgroundColor: "lavender"
  },
  tableCell: {
    fontSize: 15,
    letterSpacing: 1
  },
  textField: {
    marginLeft: 20,
    marginRight: 30,
    minWidth: 229
  }
};
export default function Report() {
  const { authenticatedUser } = useContext(AuthenticatedUserContext);
  let currentDate = new Date().toISOString().slice(0, 10);
  const reportData = {
    startDate: currentDate,
    endDate: currentDate
  };

  const [isLoading, setIsLoading] = useState(true);
  const [report, setReport] = useState(reportData);
  const [announcements, setAnnouncements] = useState([]);
  const [discussionForums, setDiscussionForums] = useState([]);
  const [resourceArticles, setResourceArticles] = useState([]);
  const [displayAnnouncementReport, setDisplayAnnouncementReport] = useState(
    false
  );
  const [displayDiscussForumReport, setDisplayDiscussForumReport] = useState(
    false
  );
  const [
    displayResourceArticleReport,
    setDisplayResourceArticleReport
  ] = useState(false);

  const [fromDateBlank, setFromDateBlank] = useState(false);
  const [toDateBlank, setToDateBlank] = useState(false);

  const generateAnnouncementReport = () => {
    setIsLoading(true);
    axios
      .post("http://localhost:8080/report/announcement", report, header)
      .then(response => {
        setAnnouncements(response.data);
        setDisplayAnnouncementReport(true);
        setDisplayDiscussForumReport(false);
        setDisplayResourceArticleReport(false);
        setIsLoading(false);
      })
      .catch(error => {
        console.log(error);
      });
  };

  const generateDiscussForumReport = () => {
    setIsLoading(true);
    axios
      .post("http://localhost:8080/report/discussion-forum", report, header)
      .then(response => {
        setDiscussionForums(response.data);
        setDisplayDiscussForumReport(true);
        setDisplayAnnouncementReport(false);
        setDisplayResourceArticleReport(false);
        setIsLoading(false);
      })
      .catch(error => {
        console.log(error);
      });
  };

  const generateResourceArticlesReport = () => {
    setIsLoading(true);
    axios
      .post("http://localhost:8080/report/resource-articles", report, header)
      .then(response => {
        setResourceArticles(response.data);
        setDisplayResourceArticleReport(true);
        setDisplayDiscussForumReport(false);
        setDisplayAnnouncementReport(false);
        setIsLoading(false);
      })
      .catch(error => {
        console.log(error);
      });
  };

  const handleChange = name => event => {
    setReport({ ...report, [name]: event.target.value });
    if (event.target.value === "") {
      if (name === "startDate") setFromDateBlank(true);
      if (name === "endDate") setToDateBlank(true);
    }
    if (event.target.value !== "") {
      if (name === "startDate") setFromDateBlank(false);
      if (name === "endDate") setToDateBlank(false);
    }
  };

  return (
    <React.Fragment>
      <NavigationBar />
      <div className="row col-md-12">
        <div className="col-md-1" />
        <div className="col-md-10">
          <Paper
            style={{ marginTop: 100, padding: 10, marginBottom: 15 }}
            elevation={3}
          >
            <Typography variant="h5" gutterBottom style={{ paddingLeft: 15 }}>
              Generate Report
            </Typography>
            <TextField
              id="outlined-uncontrolled"
              label="From Created Date"
              value={report.startDate}
              style={styles.textField}
              margin="normal"
              variant="outlined"
              type="date"
              onChange={handleChange("startDate")}
            />

            <TextField
              id="outlined-uncontrolled"
              label="To Created Date"
              value={report.endDate}
              style={styles.textField}
              margin="normal"
              variant="outlined"
              type="date"
              onChange={handleChange("endDate")}
            />

            <div
              className="row col-md-12"
              style={{ marginLeft: 10, marginBottom: 15 }}
            >
              {fromDateBlank === true ? (
                <span style={{ color: "red", marginRight: 110 }}>
                  Please select From date
                </span>
              ) : (
                <span style={{ marginRight: 281 }} />
              )}
              {toDateBlank === true ? (
                <span style={{ color: "red" }}>Please select To date</span>
              ) : (
                ""
              )}
            </div>
            <div
              className="row col-md-12"
              style={{ marginTop: 17, marginLeft: 10, marginBottom: 15 }}
            >
              <Button
                variant="contained"
                color="primary"
                type="submit"
                size="large"
                onClick={generateAnnouncementReport}
              >
                Announcement Report
              </Button>
              <Button
                variant="contained"
                color="primary"
                type="submit"
                size="large"
                style={{ marginLeft: 25 }}
                onClick={generateDiscussForumReport}
              >
                Discussion Forum Report
              </Button>
              {authenticatedUser.role === "ROLE_ADMIN" && (
                <Button
                  variant="contained"
                  color="primary"
                  type="submit"
                  size="large"
                  style={{ marginLeft: 25 }}
                  onClick={generateResourceArticlesReport}
                >
                  Resource Articles Report
                </Button>
              )}
            </div>
          </Paper>
        </div>
      </div>
      {displayAnnouncementReport && (
        <section>
          <div className="row col-md-12">
            <div className="col-md-6">
              <Typography
                variant="h5"
                gutterBottom
                style={{ marginTop: 20, marginLeft: "20%" }}
              >
                Announcement Report
              </Typography>
            </div>
          </div>
          <div className="row col-md-12">
            <div className="col-md-1" />
            <div className="col-md-10">
              <Paper elevation={3}>
                <Table>
                  <TableHead style={styles.tableHeader}>
                    <TableRow>
                      <TableCell style={styles.tableCell}>
                        Created Date
                      </TableCell>
                      <TableCell style={styles.tableCell}>Created By</TableCell>
                      <TableCell style={styles.tableCell}>Username</TableCell>
                      <TableCell style={styles.tableCell}>Event Name</TableCell>
                      <TableCell style={styles.tableCell}>Date</TableCell>
                      <TableCell style={styles.tableCell}>Event Type</TableCell>
                      <TableCell style={styles.tableCell}>Notified</TableCell>
                      <TableCell style={styles.tableCell}>
                        Notified Date
                      </TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {isLoading ? (
                      <CircularProgress
                        thickness={4}
                        style={{
                          marginTop: 20,
                          marginBottom: 20,
                          marginLeft: "240%"
                        }}
                      />
                    ) : (
                      announcements.map((announcement, i) => (
                        <TableRow key={`data${i}`}>
                          <TableCell>{announcement.createdDate}</TableCell>
                          <TableCell>{`${announcement.user
                            .firstName} ${announcement.user
                            .lastName}`}</TableCell>
                          <TableCell>{announcement.user.username}</TableCell>
                          <TableCell>{announcement.eventName}</TableCell>
                          <TableCell>{announcement.eventDate}</TableCell>
                          <TableCell>{announcement.event.eventType}</TableCell>
                          <TableCell>
                            {announcement.notified ? (
                              <DoneIcon color="primary" />
                            ) : (
                              <CloseIcon color="secondary" />
                            )}
                          </TableCell>
                          <TableCell>{announcement.notifiedDate}</TableCell>
                        </TableRow>
                      ))
                    )}
                  </TableBody>
                </Table>
              </Paper>
            </div>
          </div>
        </section>
      )}

      {displayDiscussForumReport && (
        <section>
          <div className="row col-md-12">
            <div className="col-md-6">
              <Typography
                variant="h5"
                gutterBottom
                style={{ marginTop: 20, marginLeft: "20%" }}
              >
                Discussion Forum Report
              </Typography>
            </div>
          </div>
          <div className="row col-md-12">
            <div className="col-md-1" />
            <div className="col-md-10">
              <Paper elevation={3}>
                <Table>
                  <TableHead style={styles.tableHeader}>
                    <TableRow>
                      <TableCell style={styles.tableCell}>
                        Created Date
                      </TableCell>
                      <TableCell style={styles.tableCell}>Started By</TableCell>
                      <TableCell style={styles.tableCell}>Username</TableCell>
                      <TableCell style={styles.tableCell}>Course</TableCell>
                      <TableCell style={styles.tableCell}>
                        Discussion Topic
                      </TableCell>
                      <TableCell style={styles.tableCell}>
                        Total Replies
                      </TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {isLoading ? (
                      <CircularProgress
                        thickness={4}
                        style={{
                          marginTop: 20,
                          marginBottom: 20,
                          marginLeft: "240%"
                        }}
                      />
                    ) : (
                      discussionForums.map((discussionForum, i) => (
                        <TableRow key={`data${i}`}>
                          <TableCell>{discussionForum.createdDate}</TableCell>
                          <TableCell>{`${discussionForum.user
                            .firstName} ${discussionForum.user
                            .lastName}`}</TableCell>
                          <TableCell>{discussionForum.user.username}</TableCell>
                          <TableCell>
                            {discussionForum.course.courseName}
                          </TableCell>
                          <TableCell>{discussionForum.subject}</TableCell>

                          <TableCell>{discussionForum.totalPosts}</TableCell>
                        </TableRow>
                      ))
                    )}
                  </TableBody>
                </Table>
              </Paper>
            </div>
          </div>
        </section>
      )}
      {authenticatedUser.role === "ROLE_ADMIN" &&
        displayResourceArticleReport && (
          <section>
            <div className="row col-md-12">
              <div className="col-md-6">
                <Typography
                  variant="h5"
                  gutterBottom
                  style={{ marginTop: 20, marginLeft: "20%" }}
                >
                  Resource Articles Report
                </Typography>
              </div>
            </div>
            <div className="row col-md-12">
              <div className="col-md-1" />
              <div className="col-md-10">
                <Paper elevation={3}>
                  <Table>
                    <TableHead style={styles.tableHeader}>
                      <TableRow>
                        <TableCell style={styles.tableCell}>
                          Created Date
                        </TableCell>
                        <TableCell style={styles.tableCell}>
                          Created By
                        </TableCell>
                        <TableCell style={styles.tableCell}>Username</TableCell>
                        <TableCell style={styles.tableCell}>
                          Resource Type
                        </TableCell>
                        <TableCell style={styles.tableCell}>
                          Published Date
                        </TableCell>
                        <TableCell style={styles.tableCell}>Title</TableCell>
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      {isLoading ? (
                        <CircularProgress
                          thickness={4}
                          style={{
                            marginTop: 20,
                            marginBottom: 20,
                            marginLeft: "240%"
                          }}
                        />
                      ) : (
                        resourceArticles.map((resourceArticle, i) => (
                          <TableRow key={`data${i}`}>
                            <TableCell>{resourceArticle.createdDate}</TableCell>
                            <TableCell>{`${resourceArticle.user
                              .firstName} ${resourceArticle.user
                              .lastName}`}</TableCell>
                            <TableCell>
                              {resourceArticle.user.username}
                            </TableCell>
                            <TableCell>
                              {resourceArticle.resourceType}
                            </TableCell>
                            <TableCell>
                              {resourceArticle.publishedDate}
                            </TableCell>
                            <TableCell>{resourceArticle.title}</TableCell>
                          </TableRow>
                        ))
                      )}
                    </TableBody>
                  </Table>
                </Paper>
              </div>
            </div>
          </section>
        )}
    </React.Fragment>
  );
}
