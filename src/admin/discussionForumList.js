import React, { useState, useEffect, useContext } from "react";
import { Link } from "react-router-dom";

import {
  Table,
  TableBody,
  TableCell,
  TableRow,
  TableHead,
  Paper,
  Typography,
  Divider,
  IconButton,
  CircularProgress,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Button
} from "@material-ui/core";
import Fab from "@material-ui/core/Fab";

import AddIcon from "@material-ui/icons/Add";
import { Create, Delete, Comment } from "@material-ui/icons";
import axios from "axios";
import NavigationBar from "./navigationbar";

import { header } from "../authenticate/header";
import { AuthenticatedUserContext } from "../authenticate/authenticatedUserContext";

const styles = {
  tableHeader: {
    backgroundColor: "lavender"
  },
  tableCell: {
    fontSize: 15,
    letterSpacing: 1
  }
};
export default function DiscussionForumList() {
  const { authenticatedUser } = useContext(AuthenticatedUserContext);
  const [isLoading, setIsLoading] = useState(true);
  const [discussionForums, setDiscussionForums] = useState([]);
  const [dialogOpen, setDialogOpen] = useState(false);
  const [deleteDiscussionForumId, setDeleteDiscussionForumId] = useState();

  useEffect(() => {
    axios
      .get("http://localhost:8080/discussionForum/data", header)
      .then(response => {
        setDiscussionForums(response.data);
        setIsLoading(false);
      })
      .catch(error => {
        console.log(error);
      });
  }, []);

  const handleDialogOpen = id => {
    setDialogOpen(true);
    setDeleteDiscussionForumId(id);
  };

  const handleDialogClose = () => {
    setDialogOpen(false);
    setDeleteDiscussionForumId();
  };

  const handleDelete = () => {
    axios
      .delete(
        "http://localhost:8080/discussionForum/delete/" +
          deleteDiscussionForumId,
        header
      )
      .then(response => {
        console.log("Success");

        let tempDiscussionForums = discussionForums.filter(discussionForum => {
          return discussionForum.id != deleteDiscussionForumId;
        });
        setDialogOpen(false);
        setDiscussionForums(tempDiscussionForums);
      })
      .catch(error => console.log(error));
  };
  return (
    <React.Fragment>
      <NavigationBar />
      <div className="row col-md-12">
        <div className="col-md-6">
          <Typography
            variant="h4"
            gutterBottom
            style={{ marginTop: 100, marginLeft: "20%" }}
          >
            Discussion Forums
          </Typography>
        </div>

        <div className="col-md-6">
          {(authenticatedUser.role === "ROLE_ADMIN" ||
            authenticatedUser.role === "ROLE_STAFF") && (
            <Link to="/discussion-forum/create">
              <Fab
                color="primary"
                aria-label="add"
                style={{ marginTop: 80, marginLeft: "73%" }}
              >
                <AddIcon />
              </Fab>
            </Link>
          )}
        </div>
      </div>
      <div className="row col-md-12">
        <div className="col-md-1" />
        <div className="col-md-10">
          <Paper>
            <Table>
              <TableHead style={styles.tableHeader}>
                <TableRow>
                  <TableCell style={styles.tableCell}>Created Date</TableCell>
                  <TableCell style={styles.tableCell}>Course</TableCell>
                  <TableCell style={styles.tableCell}>
                    Discussion Topic
                  </TableCell>
                  <TableCell style={styles.tableCell}>Started By</TableCell>
                  <TableCell style={styles.tableCell}>Replies</TableCell>

                  <TableCell style={styles.tableCell}>
                    {`Posts ${authenticatedUser.role === "ROLE_STAFF"
                      ? "/Edit"
                      : ""}
                      ${authenticatedUser.role === "ROLE_ADMIN"
                        ? "/Edit/Delete"
                        : ""}`}
                  </TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {isLoading ? (
                  <CircularProgress
                    thickness="4"
                    style={{
                      marginTop: 20,
                      marginBottom: 20,
                      marginLeft: "240%"
                    }}
                  />
                ) : (
                  discussionForums.map((discussionForum, i) => (
                    <TableRow key={`data${i}`}>
                      <TableCell>{discussionForum.createdDate}</TableCell>
                      <TableCell>{discussionForum.course.courseName}</TableCell>
                      <TableCell>{discussionForum.subject}</TableCell>
                      <TableCell>{`${discussionForum.user
                        .firstName} ${discussionForum.user
                        .lastName}`}</TableCell>
                      <TableCell>{discussionForum.totalPosts}</TableCell>

                      <TableCell>
                        <span style={{ marginRight: 10, marginLeft: 8 }}>
                          <Link
                            to={`/discussion-forum/post/${discussionForum.id}`}
                          >
                            <Comment />
                          </Link>
                        </span>
                        {(authenticatedUser.role === "ROLE_ADMIN" ||
                          authenticatedUser.role === "ROLE_STAFF") && (
                          <span>
                            <span>|</span>
                            <span style={{ marginRight: 8, marginLeft: 8 }}>
                              <Link
                                to={`/discussion-forum/update/${discussionForum.id}`}
                              >
                                <Create />
                              </Link>
                            </span>
                          </span>
                        )}
                        {authenticatedUser.role === "ROLE_ADMIN" && (
                          <span>
                            <span>|</span>
                            <span style={{ marginLeft: 10 }}>
                              <Delete
                                style={{ color: "#007bff" }}
                                onClick={() =>
                                  handleDialogOpen(discussionForum.id)}
                              />
                            </span>
                          </span>
                        )}
                      </TableCell>
                      <Dialog
                        open={dialogOpen}
                        onClose={handleDialogClose}
                        aria-labelledby="form-dialog-title"
                      >
                        <DialogTitle id="form-dialog-title">
                          Are you sure ?
                        </DialogTitle>
                        <DialogContent>
                          <DialogContentText>
                            Do you really want to delete this record? This
                            process cannot be undone
                          </DialogContentText>
                        </DialogContent>
                        <DialogActions>
                          <Button onClick={handleDialogClose} color="primary">
                            Cancel
                          </Button>
                          <Button onClick={handleDelete} color="secondary">
                            Delete
                          </Button>
                        </DialogActions>
                      </Dialog>
                    </TableRow>
                  ))
                )}
              </TableBody>
            </Table>
          </Paper>
        </div>
      </div>
    </React.Fragment>
  );
}
