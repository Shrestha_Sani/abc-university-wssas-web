import React, { useState, useEffect, useContext } from "react";
import { Redirect } from "react-router-dom";

import axios from "axios";

import { header } from "../authenticate/header";

import {
  Typography,
  TextField,
  FormControl,
  MenuItem,
  Select,
  InputLabel,
  OutlinedInput,
  Button,
  Divider
} from "@material-ui/core";

import NavigationBar from "./navigationbar";
import { AuthenticatedUserContext } from "../authenticate/authenticatedUserContext";

const styles = {
  container: {
    display: "flex",
    flexWrap: "wrap",
    marginBottom: 35
  },
  textField: {
    marginLeft: 20,
    marginRight: 30,
    minWidth: 229
  },
  topology: {
    flex: 2
  }
};
export default function CreateUser(props) {
  const { authenticatedUser } = useContext(AuthenticatedUserContext);
  const userData = {
    id: "",
    firstName: "",
    middleName: "",
    lastName: "",
    dob: "",
    address: {
      country: "",
      city: "",
      state: "",
      address: "",
      postCode: ""
    },
    role: { id: "" },
    phoneNum: "",
    gender: "",
    degrees: [{ id: "" }],
    courses: [{ id: "" }],
    qualification: [{ id: "" }],
    email: "",
    username: "",
    password: "",
    profilePic: ""
  };

  const changePwdReqData = {
    username: authenticatedUser.username,
    currentPassword: "",
    newPassword: "",
    conformPassword: ""
  };

  const [changePwdReq, setChangePwdReq] = useState(changePwdReqData);
  const [changePwdResponse, setChangePwdResponse] = useState({});
  const [user, setUser] = useState(userData);
  const [courses, setCourses] = useState([]);
  const [degreePrograms, setDegreePrograms] = useState([]);
  const [roles, setRoles] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [navigate, setNavigation] = useState(false);
  const [showPwdError, setShowPwdError] = useState(false);

  useEffect(() => {
    axios
      .get("http://localhost:8080/course/data", header)
      .then(response => {
        setCourses(response.data);
        console.log("Course Fetched Successfully");
      })
      .catch(error => {
        console.log(error);
      });

    axios
      .get("http://localhost:8080/degree/data", header)
      .then(response => {
        setDegreePrograms(response.data);
        // setIsLoading(false);
        console.log(response.data);
        console.log("Degree Fetched Successfully");
      })
      .catch(error => {
        console.log(error);
      });

    axios
      .get("http://localhost:8080/role/data", header)
      .then(response => {
        setRoles(response.data);
        console.log("Course Fetched Successfully");
      })
      .catch(error => {
        console.log(error);
      });

    if (
      authenticatedUser.role === "ROLE_STAFF" ||
      authenticatedUser.role === "ROLE_STUDENT"
    ) {
      axios
        .get(
          "http://localhost:8080/user/username/" + authenticatedUser.username,
          header
        )
        .then(response => {
          setUser(response.data);
          // setIsLoading(false);
          console.log(response.data);
          console.log("User fetched Successfully");
        })
        .catch(error => {
          console.log(error);
        });
    }

    if (!props.id) {
      return;
    }

    axios
      .get("http://localhost:8080/user/" + props.id, header)
      .then(response => {
        setUser(response.data);
        // setIsLoading(false);
        console.log(response.data);
        console.log("User fetched Successfully");
      })
      .catch(error => {
        console.log(error);
      });
  }, []);

  const handleChange = name => event => {
    setUser({ ...user, [name]: event.target.value });
  };

  const handleChangePassword = name => event => {
    setChangePwdReq({ ...changePwdReq, [name]: event.target.value });
  };

  const handleUpdate = () => {
    let userId = props.id;
    if (
      authenticatedUser.role === "ROLE_STAFF" ||
      authenticatedUser.role === "ROLE_STUDENT"
    ) {
      userId = user.id;
    }
    axios
      .put("http://localhost:8080/user/update/" + userId, user, header)
      .then(response => {
        console.log("Success");
        setNavigation(true);
      })
      .catch(error => console.log(error));
  };

  const handleChangeNestedData = (event, parentAttribute, childAttribute) => {
    const data = {
      ...user[parentAttribute],
      [childAttribute]: event.target.value
    };
    setUser({ ...user, [parentAttribute]: data });
  };

  const handleChangeNestedData2Level = (
    event,
    parentAttribute,
    childAttribute
  ) => {
    const data = {
      ...user[parentAttribute][0],
      [childAttribute]: event.target.value
    };
    const tempCourses = [data];
    setUser({ ...user, [parentAttribute]: tempCourses });
  };

  const handleSubmit = event => {
    event.preventDefault();
    if (user.password.length < 8) {
      setShowPwdError(true);
      return;
    } else setShowPwdError(false);
    axios
      .post("http://localhost:8080/user/save", user, header)
      .then(response => {
        if (response.data.duplicateUser) {
          setUser(response.data);
          console.log("User already exist");
        } else {
          console.log("User saved Successfully");
          setUser(userData);
          setNavigation(true);
        }
      })
      .catch(error => console.log(error));
  };

  const submitPasswordChange = () => {
    if (changePwdReq.newPassword.length < 8) {
      setShowPwdError(true);
      return;
    } else setShowPwdError(false);
    console.log(changePwdReq);
    axios
      .post("http://localhost:8080/user/change-password", changePwdReq, header)
      .then(response => {
        setChangePwdResponse(response.data);
        setChangePwdReq(changePwdReqData);
      })
      .catch(error => console.log(error));
  };

  return (
    <React.Fragment>
      {navigate && (
        <div>
          {authenticatedUser.role === "ROLE_ADMIN" && (
            <Redirect to="/user/list" />
          )}
        </div>
      )}
      <div className="row col-md-12">
        <div className="col-md-8 col-md-2" />
        <NavigationBar />
        <Typography
          variant="h4"
          gutterBottom
          style={{ marginTop: 100, marginLeft: "40%" }}
        >
          User Registration
        </Typography>
        <div className="col-md-2" />
      </div>
      <div className="row col-md-12">
        <div className="col-md-2" />
        <div className="col-md-9">
          <form style={styles.container} onSubmit={handleSubmit}>
            {/************ Name *************/}
            <TextField
              required
              id="outlined-name"
              label="First Name"
              style={styles.textField}
              value={user.firstName}
              margin="normal"
              variant="outlined"
              onChange={handleChange("firstName")}
            />
            <TextField
              id="outlined-required"
              label="Middle Name"
              value={user.middleName}
              style={styles.textField}
              margin="normal"
              variant="outlined"
              onChange={handleChange("middleName")}
            />
            <TextField
              required
              id="outlined-uncontrolled"
              label="Last Name"
              value={user.lastName}
              style={styles.textField}
              margin="normal"
              variant="outlined"
              onChange={handleChange("lastName")}
            />
            {/*********** Address ***************/}
            <div className="row col-md-12">
              <Typography
                variant="overline"
                gutterBottom
                style={{ marginLeft: 18, marginTop: 7 }}
              >
                <strong>Address</strong>
              </Typography>
            </div>
            <TextField
              id="outlined-name"
              label="Country"
              style={styles.textField}
              value={user.address.country}
              margin="normal"
              variant="outlined"
              onChange={event =>
                handleChangeNestedData(event, "address", "country")}
            />
            <TextField
              required
              id="outlined-required"
              label="City"
              value={user.address.city}
              style={styles.textField}
              margin="normal"
              variant="outlined"
              onChange={event =>
                handleChangeNestedData(event, "address", "city")}
            />
            <TextField
              id="outlined-uncontrolled"
              label="State"
              value={user.address.state}
              style={styles.textField}
              margin="normal"
              variant="outlined"
              onChange={event =>
                handleChangeNestedData(event, "address", "state")}
            />
            <TextField
              required
              id="outlined-required"
              label="Address"
              value={user.address.address}
              style={styles.textField}
              margin="normal"
              variant="outlined"
              onChange={event =>
                handleChangeNestedData(event, "address", "address")}
            />
            <TextField
              id="outlined-uncontrolled"
              label="PostCode"
              value={user.address.postCode}
              style={styles.textField}
              margin="normal"
              variant="outlined"
              onChange={event =>
                handleChangeNestedData(event, "address", "postCode")}
            />
            {/***************** Other Information *****************/}
            <div className="row col-md-12">
              <Typography
                variant="overline"
                gutterBottom
                style={{ marginLeft: 18, marginTop: 7 }}
              >
                <strong>Other Information</strong>
              </Typography>
            </div>
            <TextField
              id="outlined-uncontrolled"
              label="Date of Birth"
              value={user.dob}
              style={styles.textField}
              margin="normal"
              variant="outlined"
              type="date"
              onChange={handleChange("dob")}
            />
            <FormControl
              variant="outlined"
              style={{
                minWidth: 229,
                marginTop: 14,
                marginLeft: 20,
                marginRight: 30
              }}
            >
              <InputLabel htmlFor="outlined-gender-simple">Gender</InputLabel>
              <Select
                value={user.gender}
                input={
                  <OutlinedInput name="gender" id="outlined-gender-simple" />
                }
                onChange={handleChange("gender")}
              >
                <MenuItem value="">
                  <em>None</em>
                </MenuItem>
                <MenuItem value="Male">Male</MenuItem>
                <MenuItem value="Female">Female</MenuItem>
                <MenuItem value="Other">Other</MenuItem>
              </Select>
            </FormControl>
            <TextField
              required
              id="outlined-uncontrolled"
              label="Phone Number"
              value={user.phoneNum}
              style={styles.textField}
              margin="normal"
              variant="outlined"
              onChange={handleChange("phoneNum")}
            />
            {authenticatedUser.role === "ROLE_ADMIN" && (
              <FormControl
                variant="outlined"
                style={{
                  minWidth: 229,
                  marginTop: 14,
                  marginLeft: 20,
                  marginRight: 30
                }}
              >
                <InputLabel htmlFor="outlined-role-simple">Role</InputLabel>
                <Select
                  value={user.role.id}
                  input={
                    <OutlinedInput
                      name="role"
                      id="outlined-degree-program-simple"
                    />
                  }
                  onChange={event =>
                    handleChangeNestedData(event, "role", "id")}
                >
                  <MenuItem value="">
                    <em>None</em>
                  </MenuItem>
                  {roles.map(role => (
                    <MenuItem value={role.id}>{role.roleName}</MenuItem>
                  ))}
                </Select>
              </FormControl>
            )}
            {authenticatedUser.role === "ROLE_ADMIN" && (
              <FormControl
                variant="outlined"
                style={{
                  minWidth: 229,
                  marginTop: 14,
                  marginLeft: 20,
                  marginRight: 30
                }}
              >
                <InputLabel htmlFor="outlined-gender-simple">
                  Degree Program
                </InputLabel>
                <Select
                  value={user.degrees[0] ? user.degrees[0].id : ""}
                  input={
                    <OutlinedInput
                      name="degree"
                      id="outlined-degree-program-simple"
                    />
                  }
                  onChange={event =>
                    handleChangeNestedData2Level(event, "degrees", "id")}
                >
                  <MenuItem value="">
                    <em>None</em>
                  </MenuItem>
                  {degreePrograms.map(degree => (
                    <MenuItem value={degree.id}>{degree.degreeName}</MenuItem>
                  ))}
                </Select>
              </FormControl>
            )}

            {authenticatedUser.role === "ROLE_ADMIN" && (
              <FormControl
                variant="outlined"
                style={{
                  minWidth: 229,
                  marginTop: 14,
                  marginLeft: 20,
                  marginRight: 30
                }}
              >
                <InputLabel htmlFor="outlined-course-simple">Course</InputLabel>
                <Select
                  value={user.courses[0].id}
                  input={
                    <OutlinedInput
                      name="course"
                      id="outlined-degree-program-simple"
                    />
                  }
                  onChange={event =>
                    handleChangeNestedData2Level(event, "courses", "id")}
                >
                  <MenuItem value="">
                    <em>None</em>
                  </MenuItem>
                  {courses.map(course => (
                    <MenuItem value={course.id}>
                      {`${course.courseCode} ${course.courseName}`}
                    </MenuItem>
                  ))}
                </Select>
              </FormControl>
            )}
            <TextField
              required
              id="outlined-uncontrolled"
              label="Email"
              value={user.email}
              style={styles.textField}
              margin="normal"
              variant="outlined"
              onChange={handleChange("email")}
              disabled={authenticatedUser.role === "ROLE_ADMIN" ? false : true}
            />
            <span>
              <TextField
                required
                id="outlined-uncontrolled"
                label="User name"
                value={user.username}
                style={styles.textField}
                margin="normal"
                variant="outlined"
                onChange={handleChange("username")}
                disabled={
                  authenticatedUser.role === "ROLE_ADMIN" ? false : true
                }
              />
              {user.duplicateUser ? (
                <p style={{ marginLeft: 25, color: "red" }}>
                  User name already exist
                </p>
              ) : (
                ""
              )}
            </span>
            {user.id ? (
              ""
            ) : (
              <span>
                <TextField
                  required
                  id="outlined-uncontrolled"
                  label="Password"
                  value={user.password}
                  style={styles.textField}
                  margin="normal"
                  variant="outlined"
                  onChange={handleChange("password")}
                  type="password"
                />
                {showPwdError === true ? (
                  <p style={{ marginLeft: 25, color: "red" }}>
                    Password must be of atleast 8 character
                  </p>
                ) : (
                  <p style={{ marginLeft: 25, color: "#b3a4a2" }}>
                    Hint: Password must be of atleast 8 character
                  </p>
                )}
              </span>
            )}
            <div
              className="row col-md-12"
              style={{ marginTop: 25, marginLeft: 14 }}
            >
              {user.id ? (
                <Button
                  variant="contained"
                  color="primary"
                  onClick={() => handleUpdate()}
                >
                  Update
                </Button>
              ) : (
                <Button variant="contained" color="primary" type="submit">
                  Create
                </Button>
              )}
            </div>
          </form>
          <Divider />
        </div>
      </div>
      {user.id && (
        <div className="row col-md-12">
          <div className="col-md-2" />
          <div className="col-md-4">
            <Typography
              variant="h5"
              gutterBottom
              style={{ marginTop: 30, textAlign: "center" }}
            >
              Change Password
            </Typography>

            <form>
              {changePwdResponse.changed ? (
                <Typography
                  variant="h6"
                  gutterBottom
                  style={{ textAlign: "center", color: "#009900" }}
                >
                  {changePwdResponse.message}
                </Typography>
              ) : (
                ""
              )}
              <span style={{ marginTop: 17, marginLeft: 17, marginRight: 15 }}>
                <strong> Current Password</strong>
              </span>
              <TextField
                required
                fullWidth
                id="outlined-uncontrolled"
                label="Current Password"
                value={changePwdReq.currentPassword}
                style={styles.textField}
                margin="normal"
                variant="outlined"
                onChange={handleChangePassword("currentPassword")}
                type="password"
              />
              {changePwdResponse.matched &&
                (changePwdResponse.changed ? (
                  ""
                ) : (
                  <span
                    style={{ color: "red", marginLeft: 15, marginRight: 173 }}
                  >
                    {changePwdResponse.message}
                  </span>
                ))}
              <span style={{ marginTop: 22, marginLeft: 17, marginRight: 15 }}>
                <strong> New Password</strong>

                <TextField
                  required
                  fullWidth
                  id="outlined-uncontrolled"
                  label="New Password"
                  value={changePwdReq.newPassword}
                  style={styles.textField}
                  margin="normal"
                  variant="outlined"
                  onChange={handleChangePassword("newPassword")}
                  type="password"
                />
                {showPwdError === true ? (
                  <p style={{ marginLeft: 25, color: "red" }}>
                    Password must be of atleast 8 character
                  </p>
                ) : (
                  <p style={{ marginLeft: 25, color: "#b3a4a2" }}>
                    Hint: Password must be of atleast 8 character
                  </p>
                )}
              </span>
              <span style={{ marginTop: 22, marginLeft: 17, marginRight: 15 }}>
                <strong> Conform Password</strong>
              </span>
              <TextField
                required
                fullWidth
                id="outlined-uncontrolled"
                label="Conform Password"
                value={changePwdReq.conformPassword}
                style={styles.textField}
                margin="normal"
                variant="outlined"
                onChange={handleChangePassword("conformPassword")}
                type="password"
              />
              {changePwdResponse.matched ? (
                ""
              ) : (
                <span style={{ color: "red", marginLeft: 15 }}>
                  {changePwdResponse.message}
                </span>
              )}
              <div
                className="row col-md-12"
                style={{ marginTop: 25, marginLeft: 14, marginButtom: 20 }}
              >
                <Button
                  variant="contained"
                  color="primary"
                  onClick={submitPasswordChange}
                >
                  OK
                </Button>
              </div>
            </form>
          </div>
        </div>
      )}
    </React.Fragment>
  );
}
