import React, { useState, useEffect, useContext } from "react";
import {
  Paper,
  Typography,
  TextField,
  Button,
  LinearProgress
} from "@material-ui/core";
import axios from "axios";
import staff from "../images/staff.png";

import NavigationBar from "./navigationbar";

import { header } from "../authenticate/header";
import { AuthenticatedUserContext } from "../authenticate/authenticatedUserContext";

export default function DiscussionForumPost(props) {
  const { authenticatedUser } = useContext(AuthenticatedUserContext);
  const styles = {};

  const discussionForumPostData = {
    post: "",
    user: {
      username: authenticatedUser.username
    },
    discussionForum: {
      id: ""
    }
  };
  const [discussionForum, setDiscussionForum] = useState({});
  const [discussionForumPost, setDiscussionForumPost] = useState(
    discussionForumPostData
  );
  const [dbDiscussionForumPostList, setDbDiscussionForumPostList] = useState(
    []
  );
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    axios
      .get("http://localhost:8080/discussionForum/" + props.id, header)
      .then(response => {
        setDiscussionForum(response.data);
        setIsLoading(false);
      })
      .catch(error => {
        console.log(error);
      });

    axios
      .get(
        "http://localhost:8080/discussionForumPost/discussionForum/" + props.id,
        header
      )
      .then(response => {
        setDbDiscussionForumPostList(response.data);
        console.log("Success");
        // setIsLoading(false);
      })
      .catch(error => {
        console.log(error);
      });
  }, []);

  const handleChange = name => event => {
    setDiscussionForumPost({
      ...discussionForumPost,
      [name]: event.target.value
    });
  };

  const handleSubmit = event => {
    let discussionForumPostTemp = { ...discussionForumPost };
    discussionForumPostTemp.discussionForum.id = props.id;
    axios
      .post(
        "http://localhost:8080/discussionForumPost/save",
        discussionForumPostTemp,
        header
      )
      .then(response => {
        setDbDiscussionForumPostList([
          ...dbDiscussionForumPostList,
          response.data
        ]);
        setDiscussionForumPost(discussionForumPostData);
        console.log("Success");
        // setDiscussionForumPost(discussionForumPostData);
      })
      .catch(error => console.log(error));
    event.preventDefault();
  };

  return (
    <React.Fragment>
      <NavigationBar />

      <div className="row col-md-12">
        <div className="col-md-8 offset-md-2" style={{ marginTop: 90 }}>
          {isLoading ? (
            <LinearProgress />
          ) : (
            <section>
              <Paper style={{ padding: 16, marginBottom: 10 }} elevation={3}>
                <Typography
                  variant="h4"
                  gutterBottom
                  style={{ marginBottom: 0, color: "blue" }}
                >
                  {`[${discussionForum.course.courseCode}] ${discussionForum
                    .course.courseName}`}
                </Typography>
              </Paper>
              <Paper style={{ padding: 16, marginBottom: 10 }} elevation={3}>
                <Typography
                  variant="h5"
                  gutterBottom
                  style={{ marginBottom: 0, color: "blue", marginBottom: 30 }}
                >
                  {`Discussion Forum - ${discussionForum.subject}`}
                </Typography>

                <div className="row col-md-12">
                  <div className="col-md-1">
                    <img
                      alt="Ram Kumar"
                      src={staff}
                      width={30}
                      height={30}
                      className="rounded-circle"
                    />
                  </div>

                  <div className="col-md-8">
                    <p style={{ marginBottom: 0 }}>
                      <strong>{discussionForum.subject}</strong>
                    </p>

                    <p style={{ marginBottom: 20 }}>
                      <span>Created by</span>
                      <span style={{ color: "blue" }}>
                        {` ${discussionForum.user.firstName} ${discussionForum
                          .user.lastName}`}
                      </span>
                      <span>
                        {` -
                    ${discussionForum.createdDate}`}
                      </span>
                    </p>
                    <p style={{ marginBottom: 15 }}>
                      {discussionForum.message}
                    </p>
                  </div>
                </div>
              </Paper>
              {dbDiscussionForumPostList.map((dfPost, i) => (
                <Paper
                  style={{ padding: 16, marginBottom: 10 }}
                  elevation={3}
                  key={`post${i}`}
                >
                  <div className="row col-md-12">
                    <div className="col-md-1">
                      <img
                        alt="Ram Kumar"
                        src={staff}
                        width={30}
                        height={30}
                        className="rounded-circle"
                      />
                    </div>

                    <div className="col-md-8">
                      <p style={{ marginBottom: 0 }}>
                        <strong>{`Re: ${discussionForum.subject}`}</strong>
                      </p>

                      <p style={{ marginBottom: 20 }}>
                        <span> Posted by</span>
                        <span style={{ color: "blue" }}>
                          {` ${dfPost.user.firstName} ${dfPost.user.lastName}`}
                        </span>
                        <span>
                          {` -
                    ${dfPost.postedDate}`}
                        </span>
                      </p>
                      <p style={{ marginBottom: 15 }}>{dfPost.post}</p>
                    </div>
                  </div>
                </Paper>
              ))}
              <form onSubmit={handleSubmit}>
                <TextField
                  fullWidth
                  id="outlined-required"
                  label="Reply..."
                  value={discussionForumPost.post}
                  margin="normal"
                  variant="outlined"
                  multiline={true}
                  rows={4}
                  rowsMax={6}
                  onChange={handleChange("post")}
                />
                <Button
                  variant="contained"
                  color="primary"
                  type="submit"
                  style={{ marginTop: 10, float: "right" }}
                >
                  Post To Forum
                </Button>
              </form>
            </section>
          )}
        </div>
      </div>
    </React.Fragment>
  );
}
