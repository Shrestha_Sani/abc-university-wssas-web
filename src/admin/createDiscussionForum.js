import React, { useState, useEffect, useContext } from "react";
import { Redirect } from "react-router-dom";

import axios from "axios";

import {
  Typography,
  TextField,
  FormControl,
  MenuItem,
  Select,
  InputLabel,
  OutlinedInput,
  Button,
  Paper
} from "@material-ui/core";

import NavigationBar from "./navigationbar";
import { header } from "../authenticate/header";
import { AuthenticatedUserContext } from "../authenticate/authenticatedUserContext";

const styles = {
  container: {
    display: "flex",
    flexWrap: "wrap",
    marginBottom: 35
  },
  textField: {
    marginLeft: 20,
    marginRight: 30,
    minWidth: 229
  },
  topology: {
    flex: 2
  }
};
export default function CreateDiscussionForum(props) {
  const { authenticatedUser } = useContext(AuthenticatedUserContext);
  const discussionForumData = {
    subject: "",
    message: "",
    course: {
      id: ""
    },
    user: {
      username: authenticatedUser.username
    }
  };
  const [courses, setCourses] = useState([]);
  const [discussionForum, setDiscussionForum] = useState(discussionForumData);
  const [isLoading, setIsLoading] = useState(true);
  const [navigate, setNavigation] = useState(false);

  useEffect(() => {
    axios
      .get("http://localhost:8080/course/data", header)
      .then(response => {
        setCourses(response.data);
      })
      .catch(error => {
        console.log(error);
      });

    if (!props.id) {
      return;
    }

    axios
      .get("http://localhost:8080/discussionForum/" + props.id, header)
      .then(response => {
        setDiscussionForum(response.data);
        // setIsLoading(false);
      })
      .catch(error => {
        console.log(error);
      });
  }, []);

  const handleChange = name => event => {
    setDiscussionForum({ ...discussionForum, [name]: event.target.value });
  };

  const handleUpdate = () => {
    let updatedBy = {
      ...discussionForum.updatedBy,
      username: authenticatedUser.username
    };
    let tempDiscussionForum = { ...discussionForum, updatedBy };
    axios
      .put(
        "http://localhost:8080/discussionForum/update/" + props.id,
        tempDiscussionForum,
        header
      )
      .then(response => {
        console.log("Success");
        setNavigation(true);
      })
      .catch(error => console.log(error));
  };

  const handleChangeNestedData = (event, parentAttribute, childAttribute) => {
    const data = {
      ...discussionForum[parentAttribute],
      [childAttribute]: event.target.value
    };
    setDiscussionForum({ ...discussionForum, [parentAttribute]: data });
  };

  const handleSubmit = event => {
    axios
      .post(
        "http://localhost:8080/discussionForum/save",
        discussionForum,
        header
      )
      .then(response => {
        console.log("Success");
        setDiscussionForum(discussionForumData);
        setNavigation(true);
      })
      .catch(error => console.log(error));
    event.preventDefault();
  };

  return (
    <React.Fragment>
      {navigate && (
        <div>
          <Redirect to="/discussion-forum/list" />
        </div>
      )}
      <div className="row col-md-12">
        <div className="col-md-8 col-md-2" />
        <NavigationBar />
        <Typography
          variant="h4"
          gutterBottom
          style={{ marginTop: 100, marginLeft: "40%" }}
        >
          Create Discussion Forum
        </Typography>
        <div className="col-md-2" />
      </div>
      <div className="row col-md-12">
        <div className="col-md-2" />
        <div className="col-md-9">
          <Paper style={{ padding: 27, marginRight: 25 }} elevation={3}>
            <form style={styles.container} onSubmit={handleSubmit}>
              <FormControl
                fullWidth
                variant="outlined"
                style={{
                  minWidth: 229,
                  marginTop: 14,
                  marginLeft: 20,
                  marginRight: 30
                }}
              >
                <InputLabel htmlFor="outlined-gender-simple">
                  Select Course
                </InputLabel>
                <Select
                  input={
                    <OutlinedInput name="Course" id="outlined-course-simple" />
                  }
                  value={discussionForum.course.id}
                  onChange={event =>
                    handleChangeNestedData(event, "course", "id")}
                >
                  <MenuItem value="">
                    <em>None</em>
                  </MenuItem>
                  {courses.map((data, i) => (
                    <MenuItem
                      value={data.id}
                    >{`${data.courseCode}-${data.courseName}`}</MenuItem>
                  ))}
                </Select>
              </FormControl>
              <TextField
                required
                id="outlined-name"
                label="Discussion Topic"
                style={styles.textField}
                value={discussionForum.subject}
                margin="normal"
                variant="outlined"
                onChange={handleChange("subject")}
                fullWidth
              />

              <TextField
                fullWidth
                id="outlined-required"
                label="Course Description"
                value={discussionForum.message}
                style={styles.textField}
                margin="normal"
                variant="outlined"
                multiline={true}
                rows={5}
                rowsMax={7}
                onChange={handleChange("message")}
              />
              <div
                className="row col-md-12"
                style={{ marginTop: 25, marginLeft: 14 }}
              >
                {discussionForum.id ? (
                  <Button
                    variant="contained"
                    color="primary"
                    onClick={() => handleUpdate()}
                  >
                    Update
                  </Button>
                ) : (
                  <Button variant="contained" color="primary" type="submit">
                    Create
                  </Button>
                )}
              </div>
            </form>
          </Paper>
        </div>
      </div>
    </React.Fragment>
  );
}
