import React, { useState } from "react";
import { Link } from "react-router-dom";
import {
  AppBar,
  Typography,
  IconButton,
  Toolbar,
  Drawer,
  Divider,
  MenuItem,
  Button,
  PowerSettingsNew
} from "@material-ui/core";
import MenuIcon from "@material-ui/icons/Menu";
import {
  AccountCircle,
  School,
  Event,
  Announcement,
  Forum,
  SpeakerNotes,
  Timeline,
  Backspace,
  Dashboard,
  Description,
  Assignment
} from "@material-ui/icons";
import PowerSettingsNewIcon from "@material-ui/icons/PowerSettingsNew";

import { AuthenticatedUserConsumer } from "../authenticate/authenticatedUserContext";

const styles = {
  rightToolbar: {
    marginLeft: "auto",
    marginRight: -12
  },
  menuButton: {
    marginRight: 16,
    marginLeft: -12
  },
  userName: {
    paddingLeft: 10
  },
  drawer: {
    marginLeft: 30,
    marginRight: 20
  },
  drawerMenuItemText: {
    marginLeft: 20,
    marginTop: 16
  },
  menuItem: {
    lineHeight: 0
  }
};

export default function NavigationBar() {
  const [drawerOpen, setDrawerOpen] = useState();
  const handleLogout = (e, setAuthenticatedUser) => {
    sessionStorage.removeItem("token");
    setAuthenticatedUser(prevData => ({
      ...prevData,
      username: "",
      isLoggedIn: false,
      role: "",
      errorMessage: "",
      redirectUrl: "/"
    }));
  };
  return (
    <AuthenticatedUserConsumer>
      {({ authenticatedUser, setAuthenticatedUser }) => (
        <div>
          <AppBar>
            <Toolbar>
              <IconButton
                color="inherit"
                style={styles.menuButton}
                onClick={() => setDrawerOpen(!drawerOpen)}
              >
                <MenuIcon />
              </IconButton>
              <Typography variant="h5">
                <span>ABC University</span>
              </Typography>

              <section style={styles.rightToolbar}>
                <IconButton
                  aria-controls="menu-appbar"
                  aria-haspopup="true"
                  color="inherit"
                  style={{ marginLeft: 5 }}
                >
                  <AccountCircle />

                  <Typography variant="h7" style={styles.userName}>
                    {authenticatedUser.username}
                  </Typography>
                </IconButton>
                <Button
                  color="inherit"
                  startIcon={<PowerSettingsNewIcon />}
                  onClick={e => handleLogout(e, setAuthenticatedUser)}
                >
                  <PowerSettingsNewIcon />
                  Logout
                </Button>
              </section>
            </Toolbar>
          </AppBar>
          <Drawer anchor="left" open={drawerOpen}>
            <div style={{ minHeight: 65, textAlign: "right", marginRight: 10 }}>
              <Backspace onClick={() => setDrawerOpen(!drawerOpen)} />
            </div>
            <Divider />
            <section style={styles.drawer}>
              {authenticatedUser.role === "ROLE_ADMIN" && (
                <Link to="/dashboard">
                  <MenuItem style={styles.menuItem}>
                    <Dashboard />
                    <p style={styles.drawerMenuItemText}>Dashboard</p>
                  </MenuItem>
                </Link>
              )}
              {authenticatedUser.role === "ROLE_STAFF" && (
                <Link to="/staff/dashboard">
                  <MenuItem style={styles.menuItem}>
                    <Dashboard />
                    <p style={styles.drawerMenuItemText}>Dashboard</p>
                  </MenuItem>
                </Link>
              )}
              {authenticatedUser.role === "ROLE_STUDENT" && (
                <Link to="/student/dashboard">
                  <MenuItem style={styles.menuItem}>
                    <Dashboard />
                    <p style={styles.drawerMenuItemText}>Dashboard</p>
                  </MenuItem>
                </Link>
              )}
              {authenticatedUser.role === "ROLE_ADMIN" && (
                <Link to="/user/list">
                  <MenuItem style={styles.menuItem}>
                    <AccountCircle />
                    <p style={styles.drawerMenuItemText}>User</p>
                  </MenuItem>
                </Link>
              )}
              {(authenticatedUser.role === "ROLE_STAFF" ||
                authenticatedUser.role === "ROLE_STUDENT") && (
                <Link to="/user/create">
                  <MenuItem style={styles.menuItem}>
                    <AccountCircle />
                    <p style={styles.drawerMenuItemText}>Update Profile</p>
                  </MenuItem>
                </Link>
              )}
              {authenticatedUser.role === "ROLE_ADMIN" && (
                <Link to="/course">
                  <MenuItem style={styles.menuItem}>
                    <School />
                    <p style={styles.drawerMenuItemText}>Course</p>
                  </MenuItem>
                </Link>
              )}
              {authenticatedUser.role === "ROLE_ADMIN" && (
                <Link to="/degree">
                  <MenuItem style={styles.menuItem}>
                    <School />
                    <p style={styles.drawerMenuItemText}>Degree</p>
                  </MenuItem>
                </Link>
              )}
              {(authenticatedUser.role === "ROLE_ADMIN" ||
                authenticatedUser.role === "ROLE_STAFF") && (
                <Link to="/announcement/list">
                  <MenuItem style={styles.menuItem}>
                    <Announcement />
                    <p style={styles.drawerMenuItemText}>
                      Announcement / Event
                    </p>
                  </MenuItem>
                </Link>
              )}

              <Link to="/discussion-forum/list">
                <MenuItem style={styles.menuItem}>
                  <Forum />
                  <p style={styles.drawerMenuItemText}>Discussion Forum</p>
                </MenuItem>
              </Link>
              {authenticatedUser.role === "ROLE_ADMIN" && (
                <Link to="/resource-article/list">
                  <MenuItem style={styles.menuItem}>
                    <Description />
                    <p style={styles.drawerMenuItemText}>Resource Articles</p>
                  </MenuItem>
                </Link>
              )}
              {(authenticatedUser.role === "ROLE_STUDENT" ||
                authenticatedUser.role === "ROLE_STAFF") && (
                <Link to="/resource-article/view">
                  <MenuItem style={styles.menuItem}>
                    <Description />
                    <p style={styles.drawerMenuItemText}>Resource Articles</p>
                  </MenuItem>
                </Link>
              )}
              {authenticatedUser.role === "ROLE_ADMIN" && (
                <Link to="/view-statistics">
                  <MenuItem style={styles.menuItem}>
                    <Timeline />
                    <p style={styles.drawerMenuItemText}>View Statistics</p>
                  </MenuItem>
                </Link>
              )}
              {(authenticatedUser.role === "ROLE_ADMIN" ||
                authenticatedUser.role === "ROLE_STAFF") && (
                <Link to="/report">
                  <MenuItem style={styles.menuItem}>
                    <Assignment />
                    <p style={styles.drawerMenuItemText}>Report</p>
                  </MenuItem>
                </Link>
              )}
            </section>
          </Drawer>
        </div>
      )}
    </AuthenticatedUserConsumer>
  );
}
