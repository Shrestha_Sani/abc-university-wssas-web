import service from './apiService.js';
 class FileService {
    uploadFileToServer(url,data){
        //returns Promise object
        return service.getRestClient().post(url, data);
    }
    getFileFromServer(url,fileName){
        //returns Promise object
        return service.getRestClient().get(url+fileName,{ responseType:"blob" });
    }
    extractFileName = (contentDispositionValue) => {
        var filename = "";
        if (contentDispositionValue && contentDispositionValue.indexOf('attachment') !== -1) {
            var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
            var matches = filenameRegex.exec(contentDispositionValue);
            if (matches != null && matches[1]) {
                filename = matches[1].replace(/['"]/g, '');
            }
        }
        return filename;
    }
}
export default (new FileService());