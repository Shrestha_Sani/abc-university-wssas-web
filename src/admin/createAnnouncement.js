import React, { useState, useEffect, useContext } from "react";
import { Redirect, Link } from "react-router-dom";

import axios from "axios";

import {
  Typography,
  TextField,
  FormControl,
  MenuItem,
  Select,
  InputLabel,
  OutlinedInput,
  Button,
  Paper,
  Icon
} from "@material-ui/core";

import { Create, Delete, Save } from "@material-ui/icons";

import NavigationBar from "./navigationbar";
import { header } from "../authenticate/header";
import { AuthenticatedUserContext } from "../authenticate/authenticatedUserContext";

const styles = {
  container: {
    display: "flex",
    flexWrap: "wrap",
    marginBottom: 35
  },
  textField: {
    marginLeft: 20,
    marginRight: 30,
    minWidth: 229
  },
  topology: {
    flex: 2
  }
};
export default function CreateAnnouncement(props) {
  const { authenticatedUser } = useContext(AuthenticatedUserContext);
  const [currentDate, setCurrentDate] = useState(new Date());
  // ******************************API call on Component Did Mount******************************
  useEffect(() => {
    axios
      .get("http://localhost:8080/event/data", header)
      .then(response => {
        response.data.forEach(d => {
          d.disableEditMode = true;
        });

        setDbEventData(response.data);
      })
      .catch(error => {
        console.log(error);
      });

    if (!props.id) {
      return;
    }

    axios
      .get("http://localhost:8080/announcement/" + props.id, header)
      .then(response => {
        setAnnouncement(response.data);
        // setIsLoading(false);
      })
      .catch(error => {
        console.log(error);
      });
  }, []);

  // *******************************Announcement**********************************************
  const announcementData = {
    id: "",
    eventName: "",
    eventDescription: "",
    eventDate: currentDate,
    event: {
      id: ""
    },
    user: {
      username: authenticatedUser.username
    }
  };

  const [announcement, setAnnouncement] = useState(announcementData);

  const handleChange = name => event => {
    setAnnouncement({ ...announcement, [name]: event.target.value });
  };

  const handleChangeNestedData = (e, parentAttribute, childAttribute) => {
    const data = {
      ...announcement[parentAttribute],
      [childAttribute]: e.target.value
    };
    setAnnouncement({ ...announcement, [parentAttribute]: data });
  };

  const handleSubmitAnnouncement = e => {
    axios
      .post("http://localhost:8080/announcement/save", announcement, header)
      .then(response => {
        console.log("Success");
        setAnnouncement(announcementData);
        setNavigation(true);
      })
      .catch(error => console.log(error));
    e.preventDefault();
  };

  const handleUpdate = () => {
    let updatedBy = {
      ...announcement.updatedBy,
      username: authenticatedUser.username
    };
    let tempAnnouncement = { ...announcement, updatedBy };
    axios
      .put(
        "http://localhost:8080/announcement/update/" + props.id,
        tempAnnouncement,
        header
      )
      .then(response => {
        console.log("Success");
        setNavigation(true);
      })
      .catch(error => console.log(error));
  };

  // **********************************************Event*************************************************

  const [eventData, setEventData] = useState([
    {
      id: "",
      eventType: ""
    }
  ]);
  const [dbEventData, setDbEventData] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [navigate, setNavigation] = useState(false);
  const handleChangeEventData = name => event => {
    setEventData({ ...eventData, [name]: event.target.value });
  };

  const handleSubmitEvent = e => {
    axios
      .post("http://localhost:8080/event/save", eventData, header)
      .then(response => {
        console.log("Success");
        setDbEventData([
          ...dbEventData,
          { ...response.data, disableEditMode: true }
        ]);
        setEventData({ ...eventData, eventType: "" });
      })
      .catch(error => console.log(error));
    e.preventDefault();
  };

  const changeEditMode = (e, index) => {
    const eData = {
      ...dbEventData[index],
      disableEditMode: !dbEventData[index].disableEditMode
    };
    let tempEventData = [...dbEventData];
    tempEventData.splice(index, 1, eData);
    setDbEventData(tempEventData);
  };

  const handleUpdateEvent = (event, id, index) => {
    const editedEventData = dbEventData.filter(data => {
      return data.id == id;
    });
    axios
      .put(
        "http://localhost:8080/event/update/" + id,
        dbEventData[index],
        header
      )
      .then(response => {
        console.log("Success");
        changeEditMode(event, index);
      })
      .catch(error => console.log(error));
  };

  const handleChangeDbEventData = (event, index) => {
    const eData = { ...dbEventData[index], eventType: event.target.value };
    const eDataTemp = [...dbEventData];
    eDataTemp.splice(index, 1, eData);
    setDbEventData(eDataTemp);
  };

  return (
    <React.Fragment>
      <NavigationBar />
      {navigate && (
        <div>
          <Redirect to="/announcement/list" />
        </div>
      )}

      <div className="row col-md-12">
        <div className="col-md-1" />
        <div className="col-md-5">
          <Paper
            style={{ padding: 25, marginTop: 100, marginRight: 25 }}
            elevation={3}
          >
            <Typography variant="h6" gutterBottom style={{ marginLeft: "30%" }}>
              Create Announcement
            </Typography>
            <form style={styles.container} onSubmit={handleSubmitAnnouncement}>
              <FormControl
                fullWidth
                variant="outlined"
                style={{
                  minWidth: 229,
                  marginTop: 14,
                  marginLeft: 20,
                  marginRight: 30
                }}
              >
                <InputLabel htmlFor="outlined-gender-simple">Event</InputLabel>
                <Select
                  input={
                    <OutlinedInput name="event" id="outlined-gender-simple" />
                  }
                  value={announcement.event.id}
                  onChange={event =>
                    handleChangeNestedData(event, "event", "id")}
                >
                  <MenuItem value="">
                    <em>None</em>
                  </MenuItem>
                  {dbEventData.map((data, i) => (
                    <MenuItem value={data.id}>{data.eventType}</MenuItem>
                  ))}
                </Select>
              </FormControl>

              <TextField
                required
                fullWidth
                id="outlined-name"
                label="Event Name"
                style={styles.textField}
                value={announcement.eventName}
                margin="normal"
                variant="outlined"
                onChange={handleChange("eventName")}
              />

              <TextField
                fullWidth
                id="outlined-uncontrolled"
                label="Event Date"
                value={announcement.eventDate}
                style={styles.textField}
                margin="normal"
                variant="outlined"
                type="date"
                onChange={handleChange("eventDate")}
              />
              <TextField
                fullWidth
                id="outlined-required"
                label="Event Description"
                value={announcement.eventDescription}
                style={styles.textField}
                margin="normal"
                variant="outlined"
                multiline={true}
                rows={4}
                rowsMax={6}
                onChange={handleChange("eventDescription")}
              />
              <div
                className="row col-md-12"
                style={{ marginTop: 25, marginLeft: 14 }}
              >
                {announcement.id ? (
                  <Button
                    variant="contained"
                    color="primary"
                    onClick={() => handleUpdate()}
                  >
                    Update
                  </Button>
                ) : (
                  <Button variant="contained" color="primary" type="submit">
                    Create
                  </Button>
                )}
              </div>
            </form>
          </Paper>
        </div>
        <div className="col-md-5">
          <Paper
            style={{ padding: 27, marginTop: 100, marginRight: 15 }}
            elevation={3}
          >
            <Typography variant="h6" gutterBottom style={{ marginLeft: "30%" }}>
              Create Event
            </Typography>
            <form style={styles.container} onSubmit={handleSubmitEvent}>
              {dbEventData.map((data, i) => (
                <section>
                  <TextField
                    id="standard-full-width"
                    label="Event"
                    style={{ margin: 8, width: 280 }}
                    placeholder="Event Name"
                    margin="normal"
                    value={data.eventType}
                    key={`data${i}`}
                    disabled={data.disableEditMode}
                    onChange={e => handleChangeDbEventData(e, i)}
                  />

                  {data.disableEditMode ? (
                    <Create
                      key={`edit${data.id}`}
                      style={{ marginTop: 28, marginRight: 5 }}
                      onClick={e => changeEditMode(e, i)}
                    />
                  ) : (
                    <Save
                      key={`save${data.id}`}
                      style={{ marginTop: 28, marginRight: 5 }}
                      onClick={e => handleUpdateEvent(e, data.id, i)}
                    />
                  )}
                  <Delete key={`delete${data.id}`} style={{ marginTop: 28 }} />
                </section>
              ))}
              <TextField
                required
                label="Event"
                style={{ margin: 8, width: 280 }}
                placeholder="Event Name"
                margin="normal"
                value={eventData.eventType}
                onChange={handleChangeEventData("eventType")}
                key={"event-raw"}
              />

              <Icon
                color="primary"
                style={{ fontSize: 40, margin: 15 }}
                onClick={handleSubmitEvent}
              >
                add_circle
              </Icon>
            </form>
          </Paper>
        </div>
      </div>
    </React.Fragment>
  );
}
