import React from "react";
import "./App.css";
import "bootstrap/dist/css/bootstrap.css";
import { Route, BrowserRouter, Switch } from "react-router-dom";
import Dashboard from "./admin/dashboard";
import CreateUser from "./admin/createUser";
import UserList from "./admin/userList";
import CreateAnnouncement from "./admin/createAnnouncement";
import AnnouncementList from "./admin/announcementList";
import CreateCourse from "./admin/createCourse";
import CreateDiscussionForum from "./admin/createDiscussionForum";
import DiscussionForumList from "./admin/discussionForumList";
import DiscussionForumPost from "./admin/discussionForumPost";
import CreateDegree from "./admin/createDegree";
import Login from "./authenticate/login";
import StudentDashboard from "./student/studentDashboard";
import StaffDashboard from "./staff/staffDashboard";
import { AuthenticatedUserProvider } from "./authenticate/authenticatedUserContext";
import ProtectedRoute from "./authenticate/protectedRoute";
import CreateResourceArticle from "./admin/createResourceArticle";
import ResourceArticleList from "./admin/resourceArticleList";
import Report from "./admin/report";
import ViewStatistics from "./admin/viewStatistics";
import ViewResourceArticles from "./admin/viewResourceArticles";

const App = (
  <BrowserRouter>
    <AuthenticatedUserProvider>
      <Switch>
        <Route exact path="/" component={Login} />
        <ProtectedRoute path="/dashboard" component={Dashboard} />
        <ProtectedRoute path="/staff/dashboard" component={StaffDashboard} />
        <ProtectedRoute
          path="/student/dashboard"
          component={StudentDashboard}
        />
        <ProtectedRoute path="/user/create" component={CreateUser} />
        <ProtectedRoute path="/user/list" component={UserList} />
        <ProtectedRoute
          path="/user/update/:id"
          render={props => <CreateUser id={props.match.params.id} />}
        />
        <ProtectedRoute
          path="/announcement/create"
          component={CreateAnnouncement}
        />
        <ProtectedRoute
          path="/announcement/list"
          component={AnnouncementList}
        />
        <ProtectedRoute
          path="/announcement/update/:id"
          render={props => <CreateAnnouncement id={props.match.params.id} />}
        />
        <ProtectedRoute path="/course" component={CreateCourse} />
        <ProtectedRoute path="/degree" component={CreateDegree} />
        <ProtectedRoute
          path="/discussion-forum/create"
          component={CreateDiscussionForum}
        />
        <ProtectedRoute
          path="/discussion-forum/update/:id"
          render={props => <CreateDiscussionForum id={props.match.params.id} />}
        />
        <ProtectedRoute
          path="/discussion-forum/list"
          component={DiscussionForumList}
        />
        />
        <ProtectedRoute
          path="/discussion-forum/post/:id"
          render={props => <DiscussionForumPost id={props.match.params.id} />}
        />
        <ProtectedRoute
          path="/resource-article/list"
          component={ResourceArticleList}
        />
        <ProtectedRoute
          path="/resource-article/create"
          component={CreateResourceArticle}
        />
        <ProtectedRoute
          path="/resource-article/update/:id"
          render={props => <CreateResourceArticle id={props.match.params.id} />}
        />
        <ProtectedRoute path="/view-statistics" component={ViewStatistics} />
        <ProtectedRoute path="/report" component={Report} />
        <ProtectedRoute
          path="/resource-article/view"
          component={ViewResourceArticles}
        />
      </Switch>
    </AuthenticatedUserProvider>
  </BrowserRouter>
);

export default App;
