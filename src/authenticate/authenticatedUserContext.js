import React, { useState, createContext } from "react";

export const AuthenticatedUserContext = createContext();

export const AuthenticatedUserProvider = props => {
  const [authenticatedUser, setAuthenticatedUser] = useState({
    username: "",
    isLoggedIn: false,
    role: "",
    redirectUrl: "/",
    errorMessage: ""
  });

  const setUsername = username => {
    setAuthenticatedUser({ ...authenticatedUser, username });
    console.log();
  };

  const setIsLoggedIn = isLoggedIn => {
    setAuthenticatedUser({ ...authenticatedUser, isLoggedIn });
  };

  return (
    <AuthenticatedUserContext.Provider
      value={{
        authenticatedUser,
        setAuthenticatedUser
      }}
    >
      {props.children}
    </AuthenticatedUserContext.Provider>
  );
};

export const AuthenticatedUserConsumer = AuthenticatedUserContext.Consumer;
