import React, { useState, useEffect, useContext } from "react";
import { Redirect } from "react-router-dom";

import axios from "axios";
import Icon from "@material-ui/core/Icon";
import TextField from "@material-ui/core/TextField";
import Checkbox from "@material-ui/core/Checkbox";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Button from "@material-ui/core/Button";
import SvgIcon from "@material-ui/core/SvgIcon";
import IconButton from "@material-ui/core/IconButton";
import { red } from "@material-ui/core/colors";

import "../App.css";
import abcLogo from "../images/ABCLogo.jpg";
import { AuthenticatedUserConsumer } from "./authenticatedUserContext";

const styles = {
  loginError: {
    backgroundColor: "rgba(220, 76, 53, 0.12)",
    color: "#800000",
    fontSize: 19,
    padding: 9,
    marginBottom: 5
  }
};

export default function Login() {
  const [credential, setCredential] = useState({});

  const handleChange = field => event => {
    setCredential({ ...credential, [field]: event.target.value });
  };

  const handleSubmit = (e, setAuthenticatedUser) => {
    e.preventDefault();
    axios
      .post("http://localhost:8080/authenticate", credential)
      .then(response => {
        let redirectUrl;
        if (response.data.role === "ROLE_ADMIN") {
          redirectUrl = "/dashboard";
        } else if (response.data.role === "ROLE_STAFF") {
          redirectUrl = "/staff/dashboard";
        } else if (response.data.role === "ROLE_STUDENT") {
          redirectUrl = "/student/dashboard";
        } else {
          redirectUrl = "/";
        }
        if (response.data.authenticated == true) {
          sessionStorage.setItem("token", response.data.jwtToken);
        }

        setAuthenticatedUser(prevData => ({
          ...prevData,
          username: response.data.username,
          isLoggedIn: response.data.authenticated,
          role: response.data.role,
          errorMessage: response.data.message,
          redirectUrl
        }));
        console.log("Successfully Loggin in");
      })
      .catch(error => console.log(error));
  };

  return (
    <AuthenticatedUserConsumer>
      {({ setAuthenticatedUser, authenticatedUser }) => (
        <div className="container">
          {authenticatedUser.isLoggedIn ? (
            <Redirect to={`${authenticatedUser.redirectUrl}`} />
          ) : (
            <div className="login-div">
              <img
                alt="Abc University"
                src={abcLogo}
                width={140}
                height={120}
                className="rounded-circle"
              />
              <Icon color="error" fontSize="large">
                lock
              </Icon>
              <header style={{ fontSize: "1.5em", margin: "0.5em" }}>
                Sign in
              </header>
              {authenticatedUser.errorMessage !== "" ? (
                <div style={styles.loginError}>
                  {authenticatedUser.errorMessage}. Please try again
                </div>
              ) : (
                ""
              )}
              <form onSubmit={e => handleSubmit(e, setAuthenticatedUser)}>
                <div>
                  <TextField
                    label="Username"
                    margin="normal"
                    variant="outlined"
                    style={{ width: "20em" }}
                    value={credential.userName}
                    onChange={handleChange("username")}
                  />
                </div>
                <div>
                  <TextField
                    label="Password"
                    margin="normal"
                    variant="outlined"
                    style={{ width: "20em" }}
                    type="password"
                    value={credential.password}
                    onChange={handleChange("password")}
                  />
                </div>
                <FormControlLabel
                  control={<Checkbox value="remember" color="primary" />}
                  label="Remember me"
                />
                <div style={{ marginTop: "1.5em" }}>
                  <Button
                    type="submit"
                    variant="contained"
                    color="primary"
                    margin="normal"
                    style={{ width: "23em" }}
                  >
                    Sign In
                  </Button>
                </div>
              </form>
            </div>
          )}
        </div>
      )}
    </AuthenticatedUserConsumer>
  );
}
