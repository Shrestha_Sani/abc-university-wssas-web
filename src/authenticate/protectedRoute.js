import React from "react";
import { Route, Redirect } from "react-router-dom";
import { AuthenticatedUserConsumer } from "./authenticatedUserContext";

const ProtectedRoute = ({ component: Component, ...rest }) => (
  <AuthenticatedUserConsumer>
    {({ authenticatedUser }) => (
      <Route
        render={props =>
          authenticatedUser.isLoggedIn ? (
            <Component {...props} />
          ) : (
            <Redirect to="/" />
          )}
        {...rest}
      />
    )}
  </AuthenticatedUserConsumer>
);

export default ProtectedRoute;
