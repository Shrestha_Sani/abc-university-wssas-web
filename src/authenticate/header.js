import React from "react";

var header = {
  headers: { Authorization: `Bearer ${sessionStorage.getItem("token")}` }
};

export { header };
