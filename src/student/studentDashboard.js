import React, { useContext, useState, useEffect } from "react";
import axios from "axios";
import {
  Typography,
  Tabs,
  Paper,
  Tab,
  Badge,
  Divider
} from "@material-ui/core";
import { AuthenticatedUserContext } from "../authenticate/authenticatedUserContext";

import StudentNavigationBar from "./studentNavigationbar";
import NavigationBar from "../admin/navigationbar";
import books from "../images/books.jpg";
import { header } from "../authenticate/header";
import { School, Notifications } from "@material-ui/icons";
import fileService from "../admin/fileService";
import { saveAs } from "file-saver";

const styles = {
  container: {
    display: "flex",
    flexWrap: "wrap",
    marginBottom: 35
  },
  box: {
    width: 362,
    height: 200,
    marginLeft: 32,

    marginBottom: 30,
    paddingLeft: 20,
    paddingTop: 10
  }
};
export default function StudentDashboard() {
  const { authenticatedUser, setAuthenticatedUser } = useContext(
    AuthenticatedUserContext
  );
  const [user, setUser] = useState({});
  const [value, setValue] = React.useState(0);
  const [notifications, setNotifications] = useState([]);
  const [news, setNews] = useState([]);

  useEffect(() => {
    axios
      .get(
        "http://localhost:8080/user/username/" + authenticatedUser.username,
        header
      )
      .then(response => {
        setUser(response.data);
      })
      .catch(error => {
        console.log(error);
      });
    axios
      .get("http://localhost:8080/announcement/notifications", header)
      .then(response => {
        setNotifications(response.data);
        console.log(response.data);
        console.log("Successfully fetched notifications");
      })
      .catch(error => {
        console.log(error);
      });
    axios
      .get("http://localhost:8080/resourceArticle/news", header)
      .then(response => {
        setNews(response.data);
        console.log(response.data);
        console.log("Successfully fetched notifications");
      })
      .catch(error => {
        console.log(error);
      });
  }, []);

  const downloadFile = fileName => {
    fileService
      .getFileFromServer("/resourceArticle/download/", fileName)
      .then(response => {
        console.log("Response", response);
        //extract file name from Content-Disposition header

        var filename = fileService.extractFileName(
          response.headers["content-disposition"]
        );
        console.log("File name", filename);
        //invoke 'Save As' dialog
        saveAs(response.data, filename);
      })
      .catch(function(error) {
        console.log(error);
        if (error.response) {
          console.log("Error", error.response.status);
        } else {
          console.log("Error", error.message);
        }
      });
  };

  const handleTabsChange = () => {
    let v = value == 0 ? 1 : 0;
    setValue(v);
  };
  return (
    <div>
      <NavigationBar />
      <div className="row col-md-12">
        <div className="col-md-2" />
        <div className="col-md-8">
          <img src={books} height="230" width="850" style={{ marginTop: 70 }} />
          <div style={{ height: 80, width: 850, backgroundColor: "#f5e9d5" }}>
            <Typography
              variant="h3"
              gutterBottom
              style={{
                marginLeft: 25,
                backgroundColor: "#f5e9d5",
                paddingTop: 15,
                letterSpacing: 2
              }}
            >
              Welcome {user.firstName}
            </Typography>
            <Paper>
              <Tabs
                value={value}
                variant="fullWidth"
                indicatorColor="primary"
                textColor="primary"
                aria-label="icon label tabs example"
                onChange={handleTabsChange}
              >
                <Tab icon={<School />} label="COURSE" />
                <Tab
                  icon={
                    <Badge badgeContent={notifications.length} color="primary">
                      <Notifications />
                    </Badge>
                  }
                  label="NOTIFICATION"
                />
              </Tabs>
              {value === 0 ? (
                <div>
                  <Typography
                    variant="h4"
                    gutterBottom
                    style={{
                      marginLeft: 30,
                      paddingTop: 35,
                      paddingBottom: 35,
                      color: "#25558f"
                    }}
                  >
                    {user.degrees && user.courses
                      ? `${user.degrees[0].degreeName} in ${user.courses[0]
                          .courseName}`
                      : ""}
                  </Typography>
                </div>
              ) : (
                <div style={{ padding: 30 }}>
                  {notifications.map((notification, i) => (
                    <div key={`notification${i}`}>
                      <strong>{notification.eventName}</strong>
                      <br />
                      <strong>Event Date:</strong> {notification.eventDate}
                      <br />
                      {notification.eventDescription}
                      <Divider style={{ marginBottom: 10, marginTop: 10 }} />
                    </div>
                  ))}
                </div>
              )}
            </Paper>
            <Paper style={{ marginTop: 30, paddingTop: 20 }} elevation={2}>
              <Typography
                variant="h4"
                gutterBottom
                style={{
                  marginLeft: 45,
                  color: "#25558f",
                  letterSpacing: 1
                }}
              >
                News
              </Typography>
              <div style={styles.container}>
                {news.map((n, i) => (
                  <Paper style={styles.box} elevation={2}>
                    <Typography
                      variant="h6"
                      gutterBottom
                      style={{
                        color: "#25558f"
                      }}
                    >
                      {n.title}
                    </Typography>

                    <p>{n.description}</p>

                    <a href="#" onClick={() => downloadFile(n.fileNames)}>
                      Read Full News
                    </a>
                  </Paper>
                ))}
              </div>
            </Paper>
          </div>
        </div>
      </div>
    </div>
  );
}
