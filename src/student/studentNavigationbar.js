import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import {
  AppBar,
  Typography,
  IconButton,
  Toolbar,
  Drawer,
  Divider,
  MenuItem,
  Badge,
  Menu
} from "@material-ui/core";
import MenuIcon from "@material-ui/icons/Menu";
import {
  AccountCircle,
  Event,
  Announcement,
  Forum,
  Dashboard,
  Notifications,
  SpeakerNotes,
  Backspace
} from "@material-ui/icons";

import { header } from "../authenticate/header";
import { AuthenticatedUserConsumer } from "../authenticate/authenticatedUserContext";

const styles = {
  rightToolbar: {
    marginLeft: "auto",
    marginRight: -12
  },
  menuButton: {
    marginRight: 16,
    marginLeft: -12
  },
  userName: {
    paddingLeft: 10
  },
  drawer: {
    marginLeft: 30,
    marginRight: 20
  },
  drawerMenuItemText: {
    marginLeft: 20,
    marginTop: 16
  },
  menuItem: {
    lineHeight: 0
  }
};

export default function StudentNavigationbar() {
  const [anchorEl, setAnchorEl] = useState(null);
  const [drawerOpen, setDrawerOpen] = useState();
  const [notifications, setNotifications] = useState([
    {
      id: "1",
      eventName: "Seminar",
      eventDescription: "jjjj kjjjj jjjjj",
      eventDate: "12-12-2020",
      event: {
        id: ""
      }
    },
    {
      id: "",
      eventName: "Training",
      eventDescription: "dddd d   ddd ",
      eventDate: "10-10-2019",
      event: {
        id: ""
      }
    }
  ]);

  const handleProfileMenuOpen = event => {
    setAnchorEl(event.currentTarget);
  };

  const isMenuOpen = Boolean(anchorEl);

  const handleMenuClose = () => {
    setAnchorEl(null);
  };

  const menuId = "primary-search-account-menu";
  const renderMenu = (
    <Menu
      anchorEl={anchorEl}
      anchorOrigin={{ vertical: "top", horizontal: "right" }}
      id={menuId}
      keepMounted
      transformOrigin={{ vertical: "top", horizontal: "right" }}
      open={isMenuOpen}
      onClose={handleMenuClose}
      style={{ marginTop: 34 }}
    >
      {notifications.map(notification => (
        <span>
          <MenuItem onClick={handleMenuClose}>
            {`${notification.eventDescription}.`}
          </MenuItem>
          <Divider />
        </span>
      ))}
    </Menu>
  );

  useEffect(() => {
    axios
      .get("http://localhost:8080/announcement/notifications", header)
      .then(response => {
        setNotifications(response.data);
        console.log("Successfully fetched notifications");
      })
      .catch(error => {
        console.log(error);
      });
  }, []);

  return (
    <AuthenticatedUserConsumer>
      {({ authenticatedUser }) => (
        <div>
          <AppBar>
            <Toolbar>
              <IconButton
                color="inherit"
                style={styles.menuButton}
                onClick={() => setDrawerOpen(!drawerOpen)}
              >
                <MenuIcon />
              </IconButton>
              <Typography variant="h5">
                <span>ABC University</span>
              </Typography>

              <section style={styles.rightToolbar}>
                <IconButton
                  aria-label="show 11 new notifications"
                  color="inherit"
                  onClick={handleProfileMenuOpen}
                  aria-controls={menuId}
                >
                  <Badge badgeContent={notifications.length} color="secondary">
                    <Notifications />
                  </Badge>
                </IconButton>
                <IconButton
                  aria-controls="menu-appbar"
                  aria-haspopup="true"
                  color="inherit"
                  style={{ marginLeft: 5 }}
                >
                  <AccountCircle />
                  <Typography variant="h6" style={styles.userName}>
                    {authenticatedUser.username}
                  </Typography>
                </IconButton>
              </section>
            </Toolbar>
          </AppBar>
          {renderMenu}
          <Drawer anchor="left" open={drawerOpen}>
            <div style={{ minHeight: 65, textAlign: "right", marginRight: 10 }}>
              <Backspace onClick={() => setDrawerOpen(!drawerOpen)} />
            </div>
            <Divider />
            <section style={styles.drawer}>
              <Link to="/user/list">
                <MenuItem style={styles.menuItem}>
                  <AccountCircle />
                  <p style={styles.drawerMenuItemText}>Update Profile</p>
                </MenuItem>
              </Link>
              <Link to="/student/dashboard">
                <MenuItem style={styles.menuItem}>
                  <Dashboard />
                  <p style={styles.drawerMenuItemText}>Dashboard</p>
                </MenuItem>
              </Link>

              <Link to="/announcement/list">
                <MenuItem style={styles.menuItem}>
                  <Announcement />
                  <p style={styles.drawerMenuItemText}>Announcement / Event</p>
                </MenuItem>
              </Link>
              <Link to="/discussion-forum/list">
                <MenuItem style={styles.menuItem}>
                  <Forum />
                  <p style={styles.drawerMenuItemText}>Discussion Forum</p>
                </MenuItem>
              </Link>
              <MenuItem style={styles.menuItem}>
                <SpeakerNotes />
                <p style={styles.drawerMenuItemText}>Resource Articles</p>
              </MenuItem>
            </section>
          </Drawer>
        </div>
      )}
    </AuthenticatedUserConsumer>
  );
}
